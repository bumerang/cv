/**
Nazev: Projekt do predmetu IPK - Pocitacove komunikace a site
       Reliable Protocol over UDP
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 15.4.2013
Modul: rdtserver.cpp
*/

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdio>
#include <vector>

#include "xml.hpp"
#include "setup_socket.hpp"

const unsigned int BUFFSIZE = 512;
const unsigned int READ_LIMIT = 350;
const int HAVE_ACK = 1;
const int MISSING_ACK = -1;

int main(int argc, char** argv)
{
    if (argc != 5)
    {
        std::cerr << "Invalid arguments\n";
        return 1;
    }

    std::string portSourceStr;
    std::string portDestStr;

    for (int i = 1; i < argc; ++i) //zpracovani parametru
    {
        if (strcmp(argv[i], "-s") == 0) //ctu cislo source portu
            portSourceStr = argv[++i];
        else if (strcmp(argv[i], "-d") == 0) //ctu cislo ciloveho portu
            portDestStr = argv[++i];
        else
        {
            std::cerr << "Invalid arguments\n";
            return 1;
        }
    }

    struct addrinfo clientAddr;
    int socketFD = SetupSocket(portSourceStr.c_str(), portDestStr.c_str(), &clientAddr);

    struct sockaddr_storage theirAddr;
    socklen_t addrLen = sizeof(theirAddr);

    char buffer[BUFFSIZE] = {'\0'};
    std::string msg;

    fcntl(socketFD, F_SETFL, O_NONBLOCK); //nastavim socket na neblokujici

    fd_set fds; //set file descriptoru
    FD_ZERO(&fds); //startovni vynulovani

    FD_SET(socketFD, &fds); //seskupim je do jednoho setu

    /*
       Poslechnu prvni segment, urcim velikost okna --> udelam buffer o teto velikosti. Ulozim segment.
       Nasloucham dalsi segmenty po dobu casovace -- jakmile vyprsi, posilam zpet ACK o jedna nizsi, nez je prvni NEpotvrzeny
       segment mimo posloupnosti. Kdyz mi prijde segment, ktery uz mam, neukladam ho. Pokud mam cele okno, vypisuji a vyprazdnuji, pak
       ?zvetsim? velikost okna. A takhle porad.
       */

    int ackNum;

    std::cout.setf(std::ios_base::unitbuf);

    while (1)
    {
        select(socketFD+1, &fds, NULL, NULL, NULL);
        if (FD_ISSET(socketFD, &fds))
        {
            ssize_t bytes = recvfrom(socketFD, buffer, BUFFSIZE-1, 0,
                                    reinterpret_cast<struct sockaddr*>(&theirAddr), &addrLen);
            if (bytes == -1)
            {
                std::cerr << "Error while receiving message\n";
                break;
            }

            msg.append(buffer, bytes);
            std::cout << GetDataFromXML(msg); //TODO: zmenit na std::cout
            ackNum = GetAckFromXML(msg);
            msg.clear();
        }
    }


    close(socketFD);

    return 0;
}
