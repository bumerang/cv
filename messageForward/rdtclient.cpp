/**
Nazev: Projekt do predmetu IPK - Pocitacove komunikace a site
       Reliable Protocol over UDP
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 15.4.2013
Modul: rdtclient.cpp
*/

#include <iostream>
#include <string>
#include <cstring>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdio>
#include <vector>

#include "xml.hpp"
#include "setup_socket.hpp"

const unsigned int READ_LIMIT = 350;
const unsigned int BUFFSIZE = 512;

int main(int argc, char** argv)
{
    //zpracovani parametru
    if (argc != 5)
    {
        std::cerr << "Invalid arguments\n";
        return 1;
    }

    std::string portSourceStr;
    std::string portDestStr;

    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "-s") == 0) //ctu cislo source portu
            portSourceStr = argv[++i];
        else if (strcmp(argv[i], "-d") == 0) //ctu cislo ciloveho portu
            portDestStr = argv[++i];
        else
        {
            std::cerr << "Invalid arguments\n";
            return 1;
        }
    }

    struct addrinfo serverAddr;
    int socketFD = SetupSocket(portSourceStr.c_str(), portDestStr.c_str(), &serverAddr); //nastavim socket
    if (socketFD == -1)
        return 1;

    fcntl(socketFD, F_SETFL, O_NONBLOCK); //nastavim socket na neblokujici
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK); //nastavim standardni vstup na neblokujici

    fd_set fds; //mnozina file descriptoru
    FD_ZERO(&fds); //startovni vynulovani

    FD_SET(STDIN_FILENO, &fds); //seskupim je do jedne mnoziny
    FD_SET(socketFD, &fds);

    std::cin >> std::noskipws; //nepreskakuj bile znaky
    std::string data;

    /*
       Beru data ze vstupu, nejak je rozsekam na segmenty, naplnim okno a budu se ho snazit poslat
       */

    struct sockaddr_storage theirAddr;
    socklen_t addrLen = sizeof(theirAddr);

    bool dataOnInput = true;
    unsigned int readed = 0;
    int serial = 0;
    int segLoaded = 0;
    bool wantAck = false;
    std::string segment;

    while (1)
    {
        if (select(socketFD+1, &fds, NULL, NULL, NULL) == -1) //zjistuji stav file descriptoru
        {
            std::cerr << "Error when checking file descriptors \n";
            return 1;
        }

        if (dataOnInput && FD_ISSET(STDIN_FILENO, &fds) && !wantAck)
        {
            char c;
            std::cin.get(c);

            if (std::cin.eof()) //konec vstupu
                dataOnInput = false;
            else
            {
                data += c;
                ++readed;
            }

            if (readed == READ_LIMIT || !dataOnInput) //vytvorim novy segment
            {
                segment = CreateDataXML(data, serial++, 0, 0);

                data.clear();
                ++segLoaded;
                readed = 0;
            }

        }
        if (FD_ISSET(socketFD, &fds) && wantAck) //mohu prijimat ACK's
        {
            if (wantAck)
            {
                char buffer[BUFFSIZE] = {'\0'};
                ssize_t bytes = recvfrom(socketFD, buffer, BUFFSIZE-1, 0,
                                         reinterpret_cast<struct sockaddr*>(&theirAddr), &addrLen);
                if (bytes == -1)
                {
                    std::cerr << "Error while receiving message\n";
                    return 1;
                }
                wantAck = false;
                std::cerr << "GOT ACK!\n";
                --segLoaded;
            }
        }
        //mohu odesilat
        if (segLoaded) //mam co odesilat
        {
            ssize_t bytes = sendto(socketFD, segment.c_str(),
                                   segment.size(), 0, serverAddr.ai_addr, serverAddr.ai_addrlen);
            if (bytes == -1)
            {
                perror(NULL);
                std::cerr << "Error when sending message\n";
                return 1;
            }
            wantAck = false;
        }

        if (! dataOnInput && !wantAck)
            return 0;

    }


    close(socketFD);

    return 0;
}
