/**
Nazev: Projekt do predmetu IPK - Pocitacove komunikace a site
       Reliable Protocol over UDP
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 17.4.2013
Modul: xml.cpp
Popis: Funkce pro praci s XML segmenty.
*/

#ifndef _XML_HPP_HEADER
#define _XML_HPP_HEADER

#include <string>
#include <sstream>

/**
Vytvori XML datovy segment protokolu.
Prijima: data (string&), seriove cislo (int), velikost okna (int), hodnota casovace v ms (unsigned int)
Vraci: datovy segment (string)
*/
std::string CreateDataXML(const std::string& data, int serial, int winSize, unsigned int tack)
{
    std::stringstream snStr; snStr << serial;
    std::stringstream winSizeStr; winSizeStr << winSize;
    std::stringstream tackStr; tackStr << tack;
    std::string result ("<rdt-segment id=\"xjirou07\">\n<header sn=\"");
    result += snStr.str();
    result += "\" ack=\"-1\" win=\"";
    result += winSizeStr.str();
    result += "\" tack=\"";
    result += tackStr.str();
    result += "\"></header>\n<data>\n";
    result += data;
    result += "</data>\n</rdt-segment>";

    return result;
}

/**
Vytvori XML acknowledge segment protokolu (zadna data).
Prijima: hodnotu ack (int)
Vraci: ack segment (string)
*/
std::string CreateAckXML(int ack)
{
    std::stringstream ackStr; ackStr << ack;
    std::string result ("<rdt-segment id=\"xjirou07\">\n<header sn=\"-1\" ack=\"");
    result += ackStr.str();
    result += "\" win=\"-1\" tack=\"0\"></header>\n<data>\n</data>\n</rdt-segment>";

    return result;
}

/**
Ziska acknowledgement (potvrzeni) udaj ze segmentu
Prijima: Segment (string&)
Vraci: Ack cislo (int)
*/
int GetAckFromXML(const std::string& segment)
{
    std::stringstream ackStr;
    size_t ackPosBeg = segment.find("ack=\"") + 5; //5 je delka ack="
    size_t ackPosEnd = segment.find('"', ackPosBeg);
    ackStr << segment.substr(ackPosBeg, ackPosEnd-ackPosBeg);

    int ack;
    ackStr >> ack;

    return ack;
}

/**
Ziska seriove cislo ze segmentu
Prijima: Segment (string&)
Vraci: seriove cislo (int)
*/
int GetSNFromXML(const std::string& segment)
{
    std::stringstream snStr;
    size_t snPosBeg = segment.find("sn=\"") + 4; //4 je delka sn="
    size_t snPosEnd = segment.find('"', snPosBeg);
    snStr << segment.substr(snPosBeg, snPosEnd-snPosBeg);

    int sn;
    snStr >> sn;

    return sn;
}

/**
Ziska velikost okna ze segmentu
Prijima: Segment (string&)
Vraci: velikost okna (int)
*/
int GetWinFromXML(const std::string& segment)
{
    std::stringstream winStr;
    size_t winPosBeg = segment.find("win=\"") + 5; //5 je delka win="
    size_t winPosEnd = segment.find('"', winPosBeg);
    winStr << segment.substr(winPosBeg, winPosEnd-winPosBeg);

    int win;
    winStr >> win;

    return win;
}

/**
Ziska hodnotu casovace ze segmentu
Prijima: Segment (string&)
Vraci: hodnotu casovace v ms (unsigned int)
*/
unsigned int GetTackFromXML(const std::string& segment)
{
    std::stringstream tackStr;
    size_t tackPosBeg = segment.find("tack=\"") + 6; //6 je delka tack="
    size_t tackPosEnd = segment.find('"', tackPosBeg);
    tackStr << segment.substr(tackPosBeg, tackPosEnd-tackPosBeg);

    unsigned int tack;
    tackStr >> tack;

    return tack;
}

/**
Ziska datovou slozku ze segmentu
Prijima: Segment (string&)
Vraci: data (sting)
*/
std::string GetDataFromXML(const std::string& segment)
{
    size_t dataPosBeg = segment.find("<data>\n") + 7; //7 je delka <data>\n
    return segment.substr(dataPosBeg, segment.rfind("</data>") - dataPosBeg);
}

#endif //_XML_HPP_HEADER
