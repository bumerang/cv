/**
Nazev: Projekt do predmetu IPK - Pocitacove komunikace a site
       Reliable Protocol over UDP
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 18.4.2013
Modul: setup_socket.hpp
*/

#ifndef _SETUP_SOCKET_HPP
#define _SETUP_SOCKET_HPP

#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdio>


//Inicializuje UDP socket a navaze ho na lokalni port
//Prijima: retezec obsahujici lokalni port (const char*)
//         retezec obsahujici cilovy port (const char*)
//         adresu pro ulozeni cilove adresy (struct addrinfo*)
//Vraci: file descriptor socketu;
//       -1 pri chybe
int SetupSocket(const char* localPortStr, const char* destPortStr, struct addrinfo* finalAddr)
{
    struct addrinfo clientAddr, *result, *rp;
    int socketFD;

    memset(&clientAddr, 0, sizeof(clientAddr));
    clientAddr.ai_family = AF_INET; //IPv4
    clientAddr.ai_socktype = SOCK_DGRAM; //UDP
    clientAddr.ai_flags = AI_PASSIVE;

    //ziskam "adresu" lokalniho portu
    int s;
    if ((s = getaddrinfo(NULL, localPortStr, &clientAddr, &result)) != 0)
    {
        std::cerr << "Address error: " << gai_strerror(s) << std::endl;
        return -1;
    }
    //vytvorim socket
    socketFD = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (socketFD == -1)
        return -1;

    //navazu socket na lokalni port
    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        if (bind(socketFD, result->ai_addr, result->ai_addrlen) != -1)
            break;
    }

    if (rp == NULL)
    {
        std::cerr << "Error during binding socket\n";
        return -1;
    }

    freeaddrinfo(result);

    struct addrinfo serverAddr;

    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.ai_family = AF_INET; //IPv4
    serverAddr.ai_socktype = SOCK_DGRAM;

    //pripojim se na cilovy port
    if ((s = getaddrinfo(NULL, destPortStr, &serverAddr, &result)) != 0)
    {
        std::cerr << "Address error: " << gai_strerror(s) << std::endl;
        return -1;
    }
/*
    for (rp = result; rp != NULL; rp = rp->ai_next)
    {

        if (connect(socketFD, rp->ai_addr, rp->ai_addrlen) != -1)
            break; //uspech, jsem pripojen

        close(socketFD);
    }

    if (rp == NULL)
    {
        std::cerr << "Error during establishing connection\n";
        return -1;
    }
*/
    *finalAddr = *result; //ukladam adresu na kterou jsem se pripojil

    return socketFD;
}

#endif //_SETUP_SOCKET_HPP
