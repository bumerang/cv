/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: steg-decode.c
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Program najde tajnou zpravu ulozenou na prvociselnych bitech
*       v souboru typu .ppm, uloha b)
*/

/**Zprava rozlustena: 6.3.2012 23:52*/

#include "bit-array.h"
#include "eratosthenes.h"
#include "error.h"
#include "ppm.h"

/**naalokovat dynamicke pole unsigned longu, vynulovat, do prvniho prvku zapsat velikost, predhodit Eratosthenovi*/
/**Osetrit chybove stavy*/

static void PrintSecretMessage(const char *filename);


int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Error("Spatny format parametru.\n");
        return EXIT_FAILURE;
    }

    PrintSecretMessage(argv[1]);

    return EXIT_SUCCESS;
}

//Vytiskne tajnou zpravu z PPM souboru, tvori ji postupne LSb na prvociselnych B
//Prijima: jmeno PPM souboru (const char *)
static void PrintSecretMessage(const char *filename)
{
    struct ppm *ppmFile = ppm_read(filename);
    if (ppmFile == NULL)
        FatalError("Chyba pri nacitani souboru.\n");

    unsigned long memberCount = 3 * ppmFile->xsize * ppmFile->ysize;

    unsigned long *array = (unsigned long *) calloc(memberCount,1);
    if (array == NULL)
        FatalError("Nedostatek pameti.\n");

    array[0] = memberCount;     //na prvnim miste (index 0) se predpoklada udaj o velikosti

    Eratosthenes(array);        //nastavi na prvociselnych indexech bity na 0

    unsigned int j = 0;
    char letter[2] = {'\0'};    //2 prvky, nebot makra na prvnim prvku predpokladaji velikost
    for (unsigned long i = 2; i < memberCount; ++i)
    {
        if (GetBit(array,i) == 0)
        {   //nastavi odpovidajici bit podle LSb bajtu s prvociselnym indexem
            DU1_SET_BIT_(letter, j, (ppmFile->data[i] & 1) );
            if (j < CHAR_BIT-1)
                ++j;
            else if (letter[1] == '\0')     //zjistena koncova nula, nepokracuji
                break;
            else
            {
                printf("%c",letter[1]);     //nacteno 8 bitu, vytiskni je jako char
                j = 0;
            }
        }
    }
}
