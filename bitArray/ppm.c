/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: ppm.c
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Modul zpracovavajici soubory ve formatu PPM, varianta P6, uloha b)
*/

#include "ppm.h"


/**dodelat kontroly zavreni souboru, alokace*/


//Nacte obsah PPM souboru do dynamicky alokovane struktury
//Prijima: jmeno souboru (const char *)
//Vraci: NULL pri chybe, jinak ukazatel na strukturu (struct ppm *)
struct ppm *ppm_read(const char *filename)
{
    FILE *fileIn = fopen(filename, "rb");  //"rb" pro jistotu, na Unix-like systemech je "b" ignorovano
    if (fileIn == NULL)
    {
        Error("Chyba pri otevirani souboru.\n");
        return NULL;
    }

    //char buffer[BUFFER_SIZE] = {0};     //optimalizovat jeho velikost
    unsigned long xSize, ySize;
    int range;
    //nacitame hlavicku souboru, na konci jeden znak ignorujeme (predpokladany whitespace)
    int check = fscanf(fileIn, "P6 %lu %lu %d%*c", &xSize, &ySize, &range);
    if (check != 3 || range != 255)
    {
        if (fclose(fileIn) == EOF)
            Error("Chyba pri zavirani souboru.\n");
        Error("Spatny format souboru.\n");
        return NULL;
    }
    //alokace potrebne struktury, alokujeme velikost samotne struktury + pocet RGB tripletu,
    //uvazujeme sizeof(char) == 1 (normou dane)
    struct ppm *ppmFile = (struct ppm*) malloc (sizeof(struct ppm) + 3*xSize*ySize);
    if (ppmFile == NULL)
    {
        if (fclose(fileIn) == EOF)
            Error("Chyba pri zavirani souboru.\n");
        Error("Nedostatek pameti.\n");
        return NULL;
    }
    //zapiseme rozmery do vytvorene struktury
    ppmFile->xsize = xSize;
    ppmFile->ysize = ySize;
    //zapiseme do data[] jednotlive R/G/B chary
    size_t count = fread(ppmFile->data, 1, 3*xSize*ySize, fileIn);
    if (count < 3*xSize*ySize)
    {
        if (fclose(fileIn) == EOF)
            Error("Chyba pri zavirani souboru.\n");
        free(ppmFile);
        Error("Chyba pri cteni souboru.\n");
        return NULL;
    }

    if (fclose(fileIn) == EOF)
            Error("Chyba pri zavirani souboru.\n");
    return ppmFile;

}

//Zapise obsah struktury do PPM souboru
//Prijima: adresu struktury (struct ppm *) a jmeno souboru (const char *)
//Vraci: -1 pri chybe, 0 pri spravnem prubehu
int ppm_write(struct ppm *ppmFile, const char *filename)
{
    FILE *fileOut = fopen(filename, "wb");
    if (fileOut == NULL)
    {
        Error("Chyba pri vytvareni souboru.\n");
        return -1;
    }
    //zapiseme hlavicku souboru, navratovou hodnotu zahazujeme (nelze ji urcit predem)
    (void) fprintf(fileOut, "P6\n%lu %lu\n255\n",ppmFile->xsize, ppmFile->ysize);

    //zapiseme data do souboru - (3*xSize*ySize) R/G/B charu
    size_t count = fwrite(ppmFile->data, 1, 3 * ppmFile->xsize * ppmFile->ysize, fileOut);
    if (count < (3* ppmFile->xsize * ppmFile->ysize) )
    {
        if (fclose(fileOut) == EOF)
            Error("Chyba pri zavirani souboru.\n");
        Error("Chyba pri zapisu do souboru.\n");
        return -1;
    }

    if (fclose(fileOut) == EOF)
            Error("Chyba pri zavirani souboru.\n");
    return 0;
}
