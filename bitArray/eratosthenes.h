/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: eratosthenes.h
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Deklarace funkce implementujici Eratosthenovo sito, uloha a),b)
*/

#ifndef ERATOSTHENES_H_INCLUDED
#define ERATOSTHENES_H_INCLUDED

#include "bit-array.h"

void Eratosthenes(BitArray_t array);

#endif // ERATOSTHENES_H_INCLUDED
