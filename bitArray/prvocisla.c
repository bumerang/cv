/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: prvocisla.c
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Hlavni soubor, uloha a)
*/
#include <stdio.h>
#include <stdlib.h>

#include "error.h"
#include "bit-array.h"
#include "eratosthenes.h"

#define NUMBER_LIMIT 99000000L     // 99 milionu, horni limit prvocisel
#define NUMBER_PRINT 10      // pocet prvocisel, ktera se maji vytisknout

static void PrimeNumbers(void);

int main(void)
{
    PrimeNumbers();

    return EXIT_SUCCESS;
}

//vytiskne poslednich NUMBER_PRINT prvocisel z intervalu 2 az NUMBER_LIMIT
static void PrimeNumbers(void)
{
    BitArray(eras, NUMBER_LIMIT);

    Eratosthenes(eras);     //index, na kterem je nulovy bit, je prvocislo
    int printed = 0;
    unsigned long i;
    for (i = NUMBER_LIMIT-1; i > 0 && printed != NUMBER_PRINT; --i)
    {   //ziskame index 10. prvocisla od konce
        if (GetBit(eras,i) == 0)
            ++printed;
    }

    for (unsigned long j = i; j < NUMBER_LIMIT; ++j)
    {   //vytiskneme prvocisla na stdout
        if (GetBit(eras,j) == 0)
            printf("%lu\n", j);
    }
}
