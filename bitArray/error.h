/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: error.h
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Deklarace funkci pro hlaseni chyb, uloha a)/b)
*/
#ifndef ERROR_H_INCLUDED
#define ERROR_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

void Error(const char *fmt, ...);
void FatalError(const char *fmt, ...);

#endif // ERROR_H_INCLUDED
