/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: ppm.h
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Rozhrani pro modul zpracovavajici soubory ve formatu PPM, varianta P6, uloha b)
*/

#ifndef PPM_H_INCLUDED
#define PPM_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#include "eratosthenes.h"
#include "bit-array.h"
#include "error.h"

//struktura pro uchovani PPM souboru
struct ppm {
    unsigned long xsize;
    unsigned long ysize;
    char data[];
};

struct ppm *ppm_read(const char *filename);

int ppm_write(struct ppm *p, const char *filename);

#endif // PPM_H_INCLUDED
