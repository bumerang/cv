/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: error.c
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Definice funkci pro hlaseni chyb, uloha a),b)
*/
#include "error.h"

//Tisk chyby na stderr
//Prijima: formatovaci retezec (const char *) ve stejnem formatu jako prijima
//         napr. funkce printf a dale prijima variabilni pocet parametru
void Error(const char *fmt, ...)
{
    fprintf(stderr,"CHYBA: ");
    va_list arg;        //inicializace
    va_start(arg, fmt);
    vfprintf(stderr, fmt, arg);
    va_end(arg);
}

//Tisk chyby na stderr s naslednym ukoncenim programu (navratova hodnota 1)
//Prijima: formatovaci retezec (const char *) ve stejnem formatu jako prijima
//         napr. funkce printf a dale prijima variabilni pocet parametru
void FatalError(const char *fmt, ...)
{
    fprintf(stderr, "CHYBA: ");

    va_list arg;
    va_start(arg, fmt);
    vfprintf(stderr, fmt, arg);
    va_end(arg);

    exit(1);
}
