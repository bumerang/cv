/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: bit-array.h
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Definice maker a inline funkci pro operace s bitovymi poli, uloha a),b)
*/

/**Vysledek: Inline funkce jsou pri zapnute optimalizaci -O2 nepatrne rychlejsi
*            oproti makrum - 2,3s vs 2,5s (Core 2 Duo, 1,71 GHz, Linux_x64)
*/


#include "error.h"
#include <limits.h>

#ifndef BIT_ARRAY_H_INCLUDED
#define BIT_ARRAY_H_INCLUDED

//pole unsigned long prvku, pro radostnejsi pouzivani
typedef unsigned long BitArray_t[];

//vrati velikost bitoveho pole (pocet bitu)
#define BitArraySize(name) (name[0])

//pocet bitu v unsigned long (CHAR_BIT(pocet bitu v char-u) == 8 na vetsine rozumnych systemu )
#define UL_BITS (CHAR_BIT*sizeof(unsigned long))

//pocet bitu v datovem typu pole predanem parametrem (CHAR_BIT == 8 na vetsine rozumnych systemu)
#define UN_TYPE_BITS(array) ( CHAR_BIT*sizeof(array[0]) )

//pocet bloku nutnych k alokaci pro dany pocet bitu (size)
#define BA_UNITS(size) ( (size) % UL_BITS != 0 ? (size)/UL_BITS + 1 : (size)/UL_BITS )

//alokuje pamet pro dany pocet bitu + inicializuje vsechny prvky na nulu
//prvni prvek pole (index 0) obsahuje jeho velikost
#define BitArray(name, size) unsigned long name[1 + BA_UNITS(size)] = {(unsigned long)(size)}

//vrati hodnotu (typ int) pozadovaneho bitu, bez kontroly mezi,
//datovy typ si zjisti automaticky
#define DU1_GET_BIT_(array, index) ( (array[1 +(index)/UN_TYPE_BITS(array)] & (1UL << (index)%UN_TYPE_BITS(array))) > 0 ? 1 : 0 )

//nastavi hodnotu pozadovaneho bitu, 0 => 0, <>0 => 1, bez kontroly mezi,
//datovy typ si zjisti automaticky
#define DU1_SET_BIT_(array, index, value) (value) != 0 ? (array[1 +(index)/UN_TYPE_BITS(array)] |= 1UL << (index)%UN_TYPE_BITS(array))\
                                                       : (array[1 +(index)/UN_TYPE_BITS(array)] &= ~(1UL << (index)%UN_TYPE_BITS(array)))


//#define USE_INLINE    //pro debugging
#ifndef USE_INLINE      //definuj makra, USE_INLINE nedefinovano



//ochrana horni meze pro makro DU1_GET_BIT_, jinak stejna funkcionalita
#define GetBit(array, index) (index) >= array[0] ? FatalError("Index %lu mimo rozsah 0..%lu", (index), array[0]-1),0\
                                                 : (DU1_GET_BIT_(array, (index)))

//ochrana horni meze pro makro DU1_SET_BIT_, jinak stejna funkcionalita
#define SetBit(array, index, value) (index) >= array[0] ? FatalError("Index %lu mimo rozsah 0..%lu", (index), array[0]-1),0\
                                                        : (DU1_SET_BIT_(array, (index), (value)))



#else       //definuj inline funkce, USE_INLINE nedefinovano


//vrati hodnotu pozadovaneho bitu, s kontrolou mezi, inline f-ce
//Prijima: bitove pole (BitArray_t), index (unsigned long)
//Vraci: hodnotu bitu (int)
inline int GetBit(BitArray_t array, unsigned long index)
{
    if (index >= array[0])
        FatalError("Index %lu mimo rozsah 0..%lu", index, array[0]-1);

    return (array[ 1 + index / (UN_TYPE_BITS(array)) ] & (1UL << (index % (UN_TYPE_BITS(array)) ))) > 0UL ? 1 : 0;
}

//nastavi hodnotu pozadovaneho bitu, 0 => 0, <>0 => 1, s kontrolou mezi, inline f-ce
//Prijima: bitove pole (BitArray_t), index (unsigned ulong), hodnotu bitu (int)
inline void SetBit(BitArray_t array, unsigned long index, int value)
{
    if (index >= array[0])
        FatalError("Index %lu mimo rozsah 0..%lu", index, array[0]-1);

    if (value != 0)     //nastavit bit na 1
        array[1 + index / (UN_TYPE_BITS(array)) ] |= 1UL << index % (UN_TYPE_BITS(array));
    else                //nastavit bit na 0
        array[1 + index / (UN_TYPE_BITS(array)) ] &= ~(1UL << index % (UN_TYPE_BITS(array)) );
}
#endif // USE_INLINE


#endif // BIT_ARRAY_H_INCLUDED
