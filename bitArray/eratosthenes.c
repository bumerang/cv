/**Projekt DU1 pro predmet IJC - Jazyk C
*Soubor: eratosthenes.c
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 3/2012
*Prelozeno: gcc 4.5.2
*Popis: Definice funkce implementujici Eratosthenovo sito, uloha a),b)
*/

#include "eratosthenes.h"
#include "bit-array.h"
#include <math.h>

//Nastavi bity na prvocislenych indexech na 0
//Prijima: vynulovane bitove pole (BitArray_t)
void Eratosthenes(BitArray_t array)
{
    unsigned long limit = (unsigned long) sqrt(array[0]);   //staci projit cisla do druhe odmocniny maximalni hodnoty
    unsigned long n = 1;
    SetBit(array,0,1);  //nulu neuvazujeme
    SetBit(array,1,1);  //jednicku neuvazujeme

    for (unsigned long i = 2; i <= limit; ++i)
    {
        if (GetBit(array,i) == 0)       //nalezeno prvocislo
        {
            while ( n*i < array[0])     //vsechny nasobky prvocisla nejsou prvocislem
            {
                SetBit(array, n*i, 1);
                ++n;
            }
            n = i;
        }
    }
    SetBit(array,2,0);      //i dvojka je prvocislo
}
