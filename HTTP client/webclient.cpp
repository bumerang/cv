/**
Projekt c.1 do predmetu Pocitacove komunikace a site (IPK) - Webovy klient
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 7.2.2013
*/

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cstring>

const unsigned int BUFFER_SIZE = 1000;
const char* DEFAULT_SERVICE = "80";
const char* DEFAULT_OUT_FILE = "index.html";

//struktura pro parsovani URL
typedef struct URL_type
{
    public:
        std::string hostname;   ///< Jmeno serveru
        std::string path;       ///< Relativni cesta daneho souboru vuci serveru
        std::string fileName;   ///< Nazev vystupniho souboru
        std::string service;    ///< Cislo portu, ktere mam pouzit
        bool wholePage;         ///< Zadano jmeno souboru nebo ne
        bool valid;             ///< Chybovy priznak
} URL_t;


//hlavicky funkci
URL_t ParseURL(const char* url);
int GetDataFromServer(URL_t URL, std::string& str, std::string& head);

//main
int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cerr << "Invalid arguments\n\tUsage: webclient URL (including http://)" << std::endl;
        return 1;
    }

    URL_t URL;
    std::string answer;
    std::string head;
    std::string urlString = argv[1];
    unsigned int redirectCounter = 0;
    bool goOn;

    //check exit code
    do
    {
        goOn = false;
        URL = ParseURL(urlString.c_str());
        if (URL.valid == false)
        {
            std::cerr << "Invalid URL or port" << std::endl;
            return 1;
        }

        //poslu pozadavek na server, ulozim si odpoved
        if (GetDataFromServer(URL, answer, head) != 0)
            return 1;

        //presmerovani
        if (head.find("301 Moved Permanently") != std::string::npos || head.find("302 Found") != std::string::npos)
        {
            ++redirectCounter;
            if (redirectCounter > 5)
            {
                std::cerr << "Error: Too many redirections (limit is 5)" << std::endl;
                return 1;
            }
            size_t where = head.find("Location:\x20") + 10; //10 je delka reretzce
            urlString.clear();
            urlString = head.substr(where, head.find("\r\n",where,2) - where);
            goOn = true;
        }

    } while (goOn);

    //status neni OK
    if (head.find("200 OK") == std::string::npos)
    {
        std::cerr << "An error has occured: " << head.substr(head.find(' ')+1, head.find("\r\n")-head.find(' ')) << std::endl;
        return 1;
    }

    //zjistim, zda slo o chunked prenos
    bool chunked;
    if (head.find("Transfer-Encoding: chunked") != std::string::npos) //chunked prenos
        chunked = true;
    else
        chunked = false;


    std::ofstream outFile;
    if (URL.wholePage == true)
        outFile.open(DEFAULT_OUT_FILE, std::ofstream::binary|std::ofstream::out);
    else
        outFile.open(URL.fileName.c_str(), std::ofstream::binary|std::ofstream::out);

    if (chunked)
    {
        std::stringstream ss (answer, std::stringstream::in|std::stringstream::out);
        unsigned int dataSize;
        do
        {
            ss >> std::hex >> dataSize; //prectu si velikost chunku
            answer.erase(0,answer.find("\r\n")+2);

            outFile.write(answer.c_str(), dataSize); //zapisu dany chunk
            answer.erase(0,dataSize); //vymazu chunk
            answer.erase(0,answer.find("\r\n")+2); //vymazu velikost dalsiho chunku
            ss.ignore(dataSize+3); //vymazu chunk z data streamu + ukonceni radku (\r\n)
        } while (dataSize != 0);
    }
    else
        outFile.write(answer.c_str(), answer.size());

    outFile.close();

    return 0;
}

/**
Odesle pozadavek na server a precte odpoved.
@param URL URL v odpovidajici strukture (URL_t)
@param str Odkaz na retezec, kam se ma zapsat odpoved serveru (std::string&)
@param head Odkaz na retezec, kam se ma zapsat hlavicka odpovedi (std::string&)
@return Chybovy stav (0 = OK)
*/
int GetDataFromServer(URL_t URL, std::string& str, std::string &head)
{
    struct addrinfo hints;
    struct addrinfo *result;
    str.clear(); //vycistim predany retezec

    memset(&hints, 0, sizeof(struct addrinfo)); //vynulovani
    hints.ai_family = AF_INET; //IPv4
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = IPPROTO_TCP; //pouzivam protokol TCP/IP

    //pokud neziskam info, dana domena neexistuje
    int s = getaddrinfo(URL.hostname.c_str(), URL.service.c_str(), &hints, &result);
    if (s != 0)
    {
        std::cerr << "Address error: " << gai_strerror(s) << std::endl;
        return 1;
    }

    int socketFD;
    struct addrinfo *rp;

    //pripojuji se na ziskane adresy, dokud neprojdu vsechny nebo se nepripojim
    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        socketFD = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (socketFD == -1)
            continue;

        if (connect(socketFD, rp->ai_addr, rp->ai_addrlen) != -1)
            break; //success

        close(socketFD);
    }

    if (rp == NULL)
    {
        std::cerr << "Connection error\n";
        close(socketFD);
        return 1;
    }

    freeaddrinfo(result); //uvolnim adresovou strukturu


    //vytvarim zpravu pozadavku, odstranuji koncove nuly (jinak selhani -- pozadavek uvazne)
    std::string message;
    message.append("GET\x20", 4); // \x20 == ' ' (mezera)
    message += URL.path;
    message.append("\x20HTTP/1.1\r\nHost:\x20",16);
    message += URL.hostname;
    message.append("\r\n\r\n",4);

    //poslu zpravu na server
    if (write(socketFD, message.data(), message.size()) != (ssize_t) message.size())
    {
        std::cerr << "Error during sending message to the server\n";
        close(socketFD);
        return 1;
    }

    char buffer[BUFFER_SIZE] = {'\0'};
    ssize_t nread;


    std::string temp;
    bool headUnreaded = true;
    bool chunked = false;
    bool notChunked = false;
    unsigned int bytesToRead = 1;
    unsigned int bytDec = 0;
    unsigned int readedBytes = 0;
    unsigned int allBytesToRead = 0;

    //celou hlavicku ctu po Bytech, pote si vyzadam presnou velikost dalsich dat (velikost cele zpravy nebo dalsiho chunku)
    while ((nread = read(socketFD, buffer, (bytesToRead > BUFFER_SIZE ? ++bytDec,BUFFER_SIZE : bytesToRead))))
    {
        if (notChunked)
        {
            readedBytes += nread;

            if (readedBytes == allBytesToRead) //precetl jsem celou zpravu
                bytesToRead = 0;
        }

        if (bytDec) //chci precist vice nez je velikost bufferu, musim nacist postupne
        {
            bytesToRead -= BUFFER_SIZE;
            --bytDec;
        }

        if (nread == -1) //chyba
        {
            std::cerr << "Error during reading data from server\n";
            close(socketFD);
            return 1;
        }
        else //vlozim prectena data do docasneho retezce
        {
            temp.append(buffer,nread);
        }

        if (chunked) //chunked prenos
        {
            //precetl jsem vsechna data
            if (temp.compare((temp.size() > 5 ? temp.size()-5 : 0), std::string::npos, "\r\n0\r\n") == 0)
            {
                bytesToRead = 0;
            }
        }

        if (headUnreaded && temp.find("\r\n\r\n") != std::string::npos) //precetl jsem hlavicku
        {
            if (temp.find("200 OK") == std::string::npos) //server nevyhovel pozadavku
            {
                bytesToRead = 0;
            }
            else if (temp.find("Transfer-Encoding: chunked") != std::string::npos) //chunked prenos
            {
                chunked = true;
            }
            else
            {
                bytesToRead = temp.find("Content-Length:") + 15; //ted slouzi jako pomocna promenna, 15 je delka retezce
                std::stringstream ss (temp.substr(bytesToRead, temp.find("\r\n", bytesToRead)-bytesToRead));
                ss >> bytesToRead;
                allBytesToRead = bytesToRead;
                notChunked = true;
            }

            head = temp; //zapisu hlavicku
            temp.clear();
            headUnreaded = false;
        }

    }

    str = temp;

    close(socketFD);

    return 0;
}

/**
Funkce rozparsuje celou URL a ulozi do odpovidajici struktury.
@param url URL adresa (const char*)
@return URL ve strukture s nastavenym chybovym priznakem (URL_t)
*/
URL_t ParseURL(const char* url)
{
    URL_t URL;
    std::string urlString (url);
    URL.valid = true;
    URL.wholePage = false;
    URL.service = DEFAULT_SERVICE;

    //kontrola spravnosti URL
    if (urlString.find("http://") != 0  )
    {
        URL.valid = false;
        return URL;
    }

    urlString.erase(0,7); //oriznu "http://"

    //oriznu zbytek retezce, zbyde jenom host
    URL.hostname = urlString.substr(0,urlString.find_first_of("/:"));

    //jmeno souboru i s relativni cestou vuci hostname
    if (urlString.find('/') == std::string::npos) //relativni cesta neexistuje
        URL.path = "/";
    else
        URL.path = urlString.substr(urlString.find('/'));

    //kontroluji, zda je pripadne cislo portu spravne
    if (urlString[URL.hostname.size()] == ':')
    {
        URL.service = urlString.substr(URL.hostname.size()+1, urlString.find('/') - URL.hostname.size()-1);
        if (URL.service.empty() == true)
        {
            URL.valid = false;
            return URL;
        }
    }

    //zjistuji, zda budu stahovat urcity soubor nebo celou stranku
    if (URL.path[URL.path.size()-1] == '/') //budu stahovat celou stranku
        URL.wholePage = true;
    else
    {
        URL.fileName = URL.path.substr(URL.path.find_last_of('/')+1); //ulozim si jmeno souboru
        for (size_t i = 0; i < URL.path.size(); ++i)
        {
            if (URL.path[i] == ' ')
                URL.path.replace(i, 1, "%20", 3);
        }
    }

    return URL;
}
