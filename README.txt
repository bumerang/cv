This repository contains projects I have worked on to prove
my experience with desired technologies.

IMPORTANT NOTE: As my programming skills grow, there is a high possibility that
    I would create some of the projects (especially the older ones) using different
    approach and coding constructs. This projects have not went through some kind
    of 'polishing'.

In future, I will move my public Git repository to the GitHub.
All source code comments are written in Czech.

Projects:
    HTTP client -- HTTP downloader, downloads object given through an argument.
        It follows up to 5 redirections and handle common HTTP responses -- 404,
        500 etc. It is implemented over TCP/IP (BSD sockets) in C++, but there 
        is no OOP paradigm used. Tested on FreeBSD and Linux.

    messageForward -- basic message forwarder, where everything on client's standard
        input is promptly send to the server, where it is printed. Connection
        is made by BSD sockets over the UDP. Written in C++, tested on FreeBSD
        and Linux.

    Falcon Language Interpreter -- team project (4 people) where I figured as a 
        team leader and main architect. Project consisted in designing and 
        implementing complete interpreter from lexical parsing through
        syntactic and semantic analysis to the interpreter itselves. There have not
        been used any generating programs (like yacc). Written in C.

        This project has its own repository: https://bitbucket.org/ppgmifj/ifj12/ 

    UserInfo -- client/server application, where server provides info about users
        on the machine where server is running. Information source is the file 
        '/etc/passwd'. Server can handle up to 50 clients (could be easily
        increased) using POSIX threads. Client is designed as lightweight, so
        all data parsing operations are performed on the server's side. 
        Program is written in C++ using TCP/IP. Tested on FreeBSD and Linux.

    readerWriter -- reader/writer synchronization problem. Program will create
        certain number (given through the program parameter) of child processes
        competing for shared resource. Written in C, tested on FreeBSD and Linux. 
        
    bitArray -- implements bit arrays with user defined length and provides 
        operations to manipulate with them. Also it uses the Sieve of Eratosthenes
        to compute last 10 prime numbers under 100 000 000. As the second
        functionality it provides program to decrypt message hidden in the 
        image (format .ppm). Bits of this message are placed on the prime number's
        position. Written in C, tested on Linux. Before running the program, it is
        critical to increase the maximum size of your stack (20K bytes are fine).

    wordcount -- count and print the number of occurences of word in given input.
        Uses hash-table to store all words. Also, it creates a static and dynamic
        linked libraries. Second part is a program that behave exactly same as
        standard linux program 'tail'. Written in C. Before using the dynamic
        linked version of the program, do not forget do set up the LD_LIBRARY_PATH
        to current directory (or move the library to the usual location).

    and many more...
