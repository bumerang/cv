/**Projekt c.2 do predmetu IOS - Operacni systemy
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 2.5.2012
*Popis: Synchronizacni problem pisaru a ctenaru s preferenci pisaru, reseno pomoci semaforu
*Modul: Rozhrani + definice
*/

#ifndef READER_WRITER_H_INCLUDED
#define READER_WRITER_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <errno.h>
#include <limits.h>
#include <time.h>

#define E_OK 0
#define E_BADPARAMS 1



//uchovani parametru
typedef struct params
{
    int errorState;
    unsigned long writersCount;
    unsigned long readersCount;
    unsigned long cycles;
    unsigned long writerSleep;
    unsigned long readerSleep;
    FILE *streamOut;
} params_t;

//prostredky pro synchronizaci
typedef struct synchReadWrt
{
    sem_t *wrt;         //pristup do sdilene pameti
    sem_t *wrtRequest;  //pozadavek na zapis
    sem_t *counter;     //pristup k citaci akci
    sem_t *readers;     //pristup k poctu ctenaru
    sem_t *wait;        //pisar ma pozadavek na zapis, ctenari musi cekat
    FILE *streamOut;    //vystupni proud
} synchReadWrt_t;


//vycisti vsechny prostredky, ktere se dedi do potomka - nevadi, pokud je nejaky argument NULL (nehrozi SEGFAULT)
#define CLEAN_CHILD do {sem_close(synch.readers);\
                        sem_close(synch.wait);\
                        sem_close(synch.wrt);\
                        sem_close(synch.wrtRequest);\
                        sem_close(synch.counter);\
                        munmap(NULL, 3*sizeof(int));\
                        shm_unlink(shmName);\
                        close(md);\
                        free(readersPID);\
                        free(writersPID);\
                        fclose(myParams.streamOut); } while (0)

//odpoji semafory, ktere timto zanikaji - nevadi, pokud je nejaky argument NULL (nehrozi SEGFAULT)
#define DESTROY_SEM do { sem_unlink(semWrtName);\
                        sem_unlink(semWrtRequestName);\
                        sem_unlink(semCounterName);\
                        sem_unlink(semReadersName);\
                        sem_unlink(semWaitName); } while(0)

/**-------------------------------------UPLNE_FUNKCI_PROTOTYPY_FUNKCI-------------------------------------*/



void writer(synchReadWrt_t synch, int *sharedMemory, unsigned long cycles, unsigned long sleepMax, int writerID);
void reader(synchReadWrt_t synch, int *sharedMemory, unsigned long sleepMax, int readerID);





#endif // READER_WRITER_H_INCLUDED

