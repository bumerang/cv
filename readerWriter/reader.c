/**Projekt c.2 do predmetu IOS - Operacni systemy
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 2.5.2012
*Popis: Synchronizacni problem pisaru a ctenaru s preferenci pisaru, reseno pomoci semaforu
*Modul: Funkce ctenare
*/


#include "readerWriter.h"



/**Funkce provadejici cinnost ctenare
*Prijima: synchronizacni strukturu (synchReadWrt_t), ukazatel na sdilenou pamet (int *), cas pro simulaci vypoctu (unsigned long),
*         sve vlastni identifikacni cislo (int)
*/
void reader(synchReadWrt_t synch, int *sharedMemory, unsigned long sleepMax, int readerID)
{
    int value = -1;

    srand(time(NULL));  //seminko pro pseudonahodne generovani

    while (value != 0)
    {

        if (sem_trywait(synch.wrtRequest) == -1)    //pokud neexistuje pozadavek na zapis, nepodari se mi zamknout (sem_trywait == -1)
        {
            sem_wait(synch.counter);    //zamykam pristup k citaci

                fprintf(synch.streamOut, "%d: reader: %d: ready\n", sharedMemory[1]++, readerID);

            sem_post(synch.counter);    //odemykam pristup k citaci

            sem_wait(synch.readers);        //zamykam pristup k poctu ctenaru

                if (sharedMemory[2]++ == 0)   //jsem prvni ctenar, zamykam pristup ke sdilene pameti
                    sem_wait(synch.wrt);

            sem_post(synch.readers);


            sem_wait(synch.counter);

                fprintf(synch.streamOut, "%d: reader: %d: reads a value\n", sharedMemory[1]++, readerID);

                value = sharedMemory[0];    //cteme hodnotu ze sdilene pameti

                fprintf(synch.streamOut, "%d: reader: %d: read: %d\n", sharedMemory[1]++, readerID, value);

            sem_post(synch.counter);

            sem_wait(synch.readers);

                if (sharedMemory[2]-- <= 1)     //jsem posledni ctenar, odemykam pristup ke sdilene pameti
                    sem_post(synch.wrt);

            sem_post(synch.readers);

            if (sleepMax != 0)
                usleep(rand() % sleepMax);  //simulujeme vypocet na nahodne generovany pocet ms
        }

        else
        {
            sem_post(synch.wrtRequest);     //nebyl pozadavek na zapis, "omylem" jsem zamknul -> odemykam

            sem_wait(synch.wait);       //cekam, dokud nezmizi pozadavek na zapis
            sem_post(synch.wait);
        }
    }
}
