/**Projekt c.2 do predmetu IOS - Operacni systemy
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 2.5.2012
*Popis: Synchronizacni problem pisaru a ctenaru s preferenci pisaru, reseno pomoci semaforu
*Modul: Hlavni modul
*/


#include "readerWriter.h"



static params_t treatParams(int argc, char **argv);


//jmena sdilenych promennych
const char *shmName = "/xjirou07_shm";
const char *semWrtName = "/xjirou07_semWrt";
const char *semWrtRequestName = "/xjirou07_semWrtRequest";
const char *semCounterName = "/xjirou07_semCounter";
const char *semReadersName = "/xjirou07_semReaders";
const char *semWaitName = "/xjirou07_semWait";


/**---------------------------------------------MAIN-------------------------------------------------------*/


int main(int argc, char **argv)
{
    params_t myParams = treatParams(argc, argv);    //zpracujeme parametry

    int EC = 0;     //navratovy kod

    if (myParams.errorState == E_BADPARAMS)
    {
        fprintf(stderr, "Spatne parametry\n");
        if (myParams.streamOut != NULL && myParams.streamOut != stdout)
            fclose(myParams.streamOut);
        return 1;   //ukoncime program, navratovy kod 1
    }


    //vypneme bufferovany vystup
    setbuf(myParams.streamOut, NULL);


    //alokace pomocnych poli pro uchovani PID potomku
    pid_t *readersPID = malloc(myParams.readersCount * sizeof(*readersPID));
    if (readersPID == NULL)
    {
        fprintf(stderr, "Nedostatek pameti\n");
        fclose(myParams.streamOut);
        return 1;
    }

    pid_t *writersPID = malloc(myParams.writersCount * sizeof(*writersPID));
    if (writersPID == NULL)
    {
        fprintf(stderr, "Nedostatek pameti\n");
        fclose(myParams.streamOut);
        free(readersPID);
        return 1;
    }

    int md = shm_open(shmName, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);    //vytvorime objekt ve sdilene pameti
    if (md < 0)
    {
        fprintf(stderr, "Nepodarilo se vytvorit objekt ve sdilene pameti.\n");
        free(readersPID);
        free(writersPID);
        fclose(myParams.streamOut);
        return 1;   //konec programu, EC = 1
    }

    if (ftruncate(md, 3*sizeof(int)) == -1)     //orizneme velikost objektu na dve int polozky - citac a sdilenou pamet
    {
        fprintf(stderr, "Chyba pri orezavani velikosti objektu ve sdilene pameti.\n");
        close(md);
        free(readersPID);
        free(writersPID);
        fclose(myParams.streamOut);
        return 1;
    }

    //!namapujeme na sdilenou pamet pole o 3 polozkach int, prvni bude samotna pamet a druha citac
    int *sharedMemory = mmap(NULL, 3*sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, md, 0);
    if (sharedMemory == MAP_FAILED)
    {
        fprintf(stderr, "Chyba pri mapovani pameti.\n");
        shm_unlink(shmName);
        close(md);
        free(readersPID);
        free(writersPID);
        fclose(myParams.streamOut);
        return 1;   //konec programu, EC = 1
    }

    //inicializace sdilene pameti
    sharedMemory[0] = -1;   //sdilena pamet
    sharedMemory[1] = 1;    //citac akci
    sharedMemory[2] = 0;    //citac ctenaru


    //vytvorime strukturu pro zapouzdreni synchronizacnich prostredku
    synchReadWrt_t synch = {.wrt = NULL, .wrtRequest = NULL, .counter = NULL, .readers = NULL, \
                            .wait = NULL, .streamOut = myParams.streamOut};

    synch.wrt = sem_open(semWrtName, O_CREAT, S_IRUSR|S_IWUSR, 1);    //vytvorime semafory inicializovane na 1
    synch.wrtRequest = sem_open(semWrtRequestName, O_CREAT, S_IRUSR|S_IWUSR, 0);    //inverzni semafor
    synch.counter = sem_open(semCounterName, O_CREAT, S_IRUSR|S_IWUSR, 1);
    synch.readers = sem_open(semReadersName, O_CREAT, S_IRUSR|S_IWUSR, 1);
    synch.wait = sem_open(semWaitName, O_CREAT, S_IRUSR|S_IWUSR, 1);

    if (synch.wrt == SEM_FAILED || synch.wrtRequest == SEM_FAILED || synch.counter == SEM_FAILED \
        || synch.readers == NULL || synch.wait == NULL)
    {
        CLEAN_CHILD;    //uvolnim prostredky

        DESTROY_SEM;    //znicim semafory
        
        EC = 1;
        fprintf(stderr, "Chyba pri vytvareni semaforu\n");
    }


    pid_t pid;  //pro identifikaci procesu

    //vytvorime pisare
    for (unsigned long i = 1; i <= myParams.writersCount; ++i)
    {
        pid = fork();   //vytvarime potomka

        if (pid < 0)    //chyba fork()
        {
            fprintf(stderr, "Chyba pri vytvareni pisarskeho potomka, koncim tvorbu.\n");
            EC = 1;     //exit code programu bude 1
            break;
        }

        if (pid == 0)   //kod pro potomka - pisare
        {
            writer(synch, sharedMemory, myParams.cycles, myParams.writerSleep, i);  //volame pisare

            CLEAN_CHILD;    //uvolnime predane prostredky

            exit(0);   //potomek konci zde
        }

        else    //kod pro rodice, pid == PID rodice
        {
            writersPID[i-1] = pid;   //uchovame si PID vsech pisaru
        }
    }

    //vytvarime ctenare
    for (unsigned long i = 1; i <= myParams.readersCount; ++i)
    {
        pid = fork();

        if (pid < 0)    //chyba fork()
        {
            fprintf(stderr, "Chyba pri vytvareni ctenarskeho potomka, koncim tvorbu.\n");
            EC = 1;     //exit code programu bude 1
            break;
        }

        if (pid == 0)   //kod pro potomka - ctenare
        {
            reader(synch, sharedMemory, myParams.readerSleep, i);   //volame ctenare

            CLEAN_CHILD;    //uvolnime predane prostredky

            exit(0);   //potomek skonci zde
        }

        else    //kod pro rodice, pid == PID potomka
        {
            readersPID[i-1] = pid;   //ukladam si PID vsech ctenaru
        }
    }

    //po skonceni vsech pisaru zapiseme do sdilene pameti 0
    int check;
    for (unsigned long i = 0; i < myParams.writersCount; ++i)
    {
        waitpid(writersPID[i], &check, 0);
    }

    sem_wait(synch.wrt);
        sharedMemory[0] = 0;    //zapiseme do pameti 0
    sem_post(synch.wrt);

    for(unsigned long i = 0; i < myParams.readersCount; ++i)    //cekame, nez skonci pisari
        waitpid(readersPID[i], &check, 0);


    CLEAN_CHILD;    //uvolnujeme sdilene polozky

    DESTROY_SEM;    //nicime semafory

    return EC;
}

/**-------------------------------------DEFINICE_FUNKCI------------------------------------------*/



static params_t treatParams(int argc, char **argv)     //predelat na for cyklus s polem unsigned long hodnot, osetrit kontrolu minus znamenka (strtoul implicitne konvertuje)
{
    params_t myParams = {.errorState = E_OK, .writersCount = 0, .readersCount = 0, .cycles = 0, \
                         .writerSleep = 0, .readerSleep = 0, .streamOut = NULL};
    if (argc != 7)
    {
        myParams.errorState = E_BADPARAMS;
        return myParams;
    }

    char lastChar = 'j';
    char *endPtr = &lastChar;
    errno = 0;

    myParams.writersCount = strtoul(argv[1], &endPtr, 0);
    if (*endPtr != '\0' || argv[1][0] == '\0' || errno == ERANGE)
    {
        myParams.errorState = E_BADPARAMS;
        return myParams;
    }

    myParams.readersCount = strtoul(argv[2], &endPtr, 0);
    if (*endPtr != '\0' || argv[2][0] == '\0' || errno == ERANGE)
    {
        myParams.errorState = E_BADPARAMS;
        return myParams;
    }

    myParams.cycles = strtoul(argv[3], &endPtr, 0);
    if (*endPtr != '\0' || argv[3][0] == '\0' || errno == ERANGE)
    {
        myParams.errorState = E_BADPARAMS;
        return myParams;
    }

    myParams.writerSleep = strtoul(argv[4], &endPtr, 0);
    if (*endPtr != '\0' || argv[4][0] == '\0' || errno == ERANGE)
    {
        myParams.errorState = E_BADPARAMS;
        return myParams;
    }

    myParams.readerSleep = strtoul(argv[5], &endPtr, 0);
    if (*endPtr != '\0' || argv[5][0] == '\0' || errno == ERANGE)
    {
        myParams.errorState = E_BADPARAMS;
        return myParams;
    }

    if (argv[6][0] == '-' && argv[6][1] == '\0')
    {
        myParams.streamOut = stdout;
        return myParams;
    }
    else
    {
        myParams.streamOut = fopen(argv[6], "w");
        if (myParams.streamOut == NULL)
        {
            myParams.errorState = E_BADPARAMS;
        }
        return myParams;
    }

    //Nikdy by nemelo nastat
    myParams.errorState = E_BADPARAMS;
    return myParams;
}

