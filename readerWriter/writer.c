/**Projekt c.2 do predmetu IOS - Operacni systemy
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 2.5.2012
*Popis: Synchronizacni problem pisaru a ctenaru s preferenci pisaru, reseno pomoci semaforu
*Modul: Funkce pisare
*/


#include "readerWriter.h"



/**Funkce provadejici cinnost pisare
*Prijima: synchronizacni strukturu (synchReadWrt_t), ukazatel na sdilenou pamet (int *), pocet zapisovacich cyklu (int),
*         cas pro simulaci vypoctu (unsigned long), sve vlastni identifikacni cislo (int)
*/
void writer(synchReadWrt_t synch, int *sharedMemory, unsigned long cycles, unsigned long sleepMax, int writerID)
{
    srand(time(NULL));      //seminko pro nahodne generovani

    for (unsigned long i = 0; i < cycles; ++i)  //provedeme cycles pocet zapisu
    {
        sem_wait(synch.counter);    //zamykam citac
            fprintf(synch.streamOut, "%d: writer: %d: new value\n", sharedMemory[1]++, writerID);
        sem_post(synch.counter);    //odemykam citac

        if (sleepMax != 0)
            usleep(rand() % sleepMax);      //simulujeme vypocet na pseudonahodne generovanou hodnotu v ms

        sem_wait(synch.counter);
            fprintf(synch.streamOut, "%d: writer: %d: ready\n", sharedMemory[1]++, writerID);
        sem_post(synch.counter);

        sem_wait(synch.wait);       //zastavime ctenare na wait semaforu

        sem_post(synch.wrtRequest);   //mame pozadavek na zapis, semafor je invertovany

        sem_wait(synch.wrt);      //zamykame sdilenou pamet pro nas zapis


        sem_wait(synch.counter);

            fprintf(synch.streamOut, "%d: writer: %d: writes a value\n", sharedMemory[1]++, writerID);

            sharedMemory[0] = writerID;     //zapiseme svoji ID do sdilene pameti

            fprintf(synch.streamOut, "%d: writer: %d: written\n", sharedMemory[1]++, writerID);

        sem_post(synch.counter);

        sem_wait(synch.wrtRequest);   //nemame pozadavek na zapis

        sem_post(synch.wait);     //uvolnime nahromadene ctenare na wait semaforu
        sem_post(synch.wrt);      //odemykame sdilenou pamet
    }

}
