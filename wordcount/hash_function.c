/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Popis: Vhodna rozptylovaci funkce pro hash tabulku, cast b)
*Poznamka: Opsano ze zadani
*/

#include "hash_function.h"

unsigned int hash_function(const char *str, unsigned htable_size)
{
    unsigned int h = 0;
    unsigned char *p;
    for (p= (unsigned char*)str; *p != '\0'; ++p)
        h = 31*h + *p;

    return h % htable_size;
}
