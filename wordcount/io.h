/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Rozhrani pro funkci, ktera nacte slovo oddelene isspace znaky ze zadaneho
*       souboru a ulozi ho do predaneho retezce, cast b)
*/

#ifndef IO_H_INCLUDED
#define IO_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

//Nacte z predaneho souboru jedno slovo, preskoci uvodni bile znake (pokud existuji)
//zapise slovo do predaneho retezce a vrati pocet nactenych znaku
//Prijima: retezec, kam ukladat znaky (char *), limit pro nacitani (int),
//         popisovac souboru, odkud ma cist (FILE *)
//Vraci: pocet nactenych znaku (int)
int fgetword(char *s, int max, FILE *f);



#endif  //IO_H_INCLUDED
