/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Implementace operaci s hashovaci tabulkou - htable_init, cast b)
*/

#include "htable.h"


/**Inicializace tabulky
*Prijima: velikost tabulky (unsigned int)
*Vraci: ukazatel na vytvorenou tabulku (htable_t *)
*       NULL pri neuspesnem vytvoreni */
htable_t *htable_init(unsigned int size)
{
    //alokujeme strukturu + polozky pomoci flexible array member (C99)
    htable_t *newTable = malloc(sizeof(htable_t) + size * sizeof(struct htable_listitem *));
    if (newTable == NULL)
        return NULL;

    //vsechny ukazatele v poli musi byt vyNULLovany
    for (unsigned int i = 0; i < size; ++i)
    {
        newTable->htable_data[i] = NULL;
    }

    newTable->htable_size = size;   //zapiseme velikost tabulky

    return newTable;
}
