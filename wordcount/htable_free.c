/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Implementace operaci s hashovaci tabulkou - htable_free, cast b)
*/

#include "htable.h"


/**Vycisti a zrusi celou tabulku
*Prijima: adresu tabulky (htable_t *)
*/
void htable_free(htable_t *tableIn)
{
    htable_clear(tableIn);  //smazeme (uvolnime) vsechny polozky v tabulce
    free(tableIn);  //uvolnime celou tabulku
    tableIn = NULL; //zahodime ukazatel
}
