/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Implementace operaci s hashovaci tabulkou - htable_remove, cast b)
*/

#include "htable.h"


/**Nalezne a odstrani polozku se stejnym klicem, pokud takova polozka neexistuje, vypise chybu na stderr
*Prijima: adresu tabulky (htable_t *)
*         klic (const char *)
*/
void htable_remove(htable_t *t, const char *key)
{
    if (t == NULL || key == NULL)
    {
        fprintf(stderr, "Chyba pri odstranovani polozky - nevalidni tabulka ci klic.\n");
        return;
    }
    unsigned int hashIndex = hash_function(key, t->htable_size);
    bool itemFound = false;

    //Prohledame tabulku
    struct htable_listitem *item = t->htable_data[hashIndex];       //zaciname na zacatku radku
    struct htable_listitem *itemPrevious = NULL;//neni zadny predchozi prvek

    while (item != NULL)    //postupne projdeme cely prislusny radek tabulky
    {
        if (strcmp(item->key, key) == 0)    //nalezli jsme prvek se stejnym klicem
        {
            itemFound = true;
            if (itemPrevious != NULL && item->next != NULL) //nalezeny prvek ma oba sousedy
                itemPrevious->next = item->next;

            else if (itemPrevious == NULL && item->next != NULL) //nalezeny prvek je prvni v radku a ma souseda
                t->htable_data[hashIndex] = item->next;

            else if (itemPrevious != NULL && item->next == NULL) //nalezeny prvek je posledni v radku
                itemPrevious->next = NULL;

            else
                t->htable_data[hashIndex] = NULL;   //nalezeny prvek je prvni a jediny v radku

            free(item->key);    //uvolnime polozku + zahodime jeji ukazatel
            item->next = NULL;
            free(item);

            break;      //prvek nalezen, netreba pokracovat (neexistuji duplicitni polozky)
        }
        itemPrevious = item;
        item = item->next;
    }
    if (itemFound == false)
        fprintf(stderr, "Polozka s klicem |%s| nenalezena.\n", key);
}
