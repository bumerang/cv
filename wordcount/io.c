/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Modul pro funkci, ktera nacte slovo oddelene isspace znaky ze zadaneho
*       souboru a ulozi ho do predaneho retezce, cast b)
*/

#include "io.h"


//Nacte z predaneho souboru jedno slovo, preskoci uvodni bile znake (pokud existuji)
//zapise slovo do predaneho retezce a vrati pocet nactenych znaku
//Prijima: retezec, kam ukladat znaky (char *), limit pro nacitani (int),
//         popisovac souboru, odkud ma cist (FILE *)
//Vraci: pocet nactenych znaku (int)
int fgetword(char *s, int max, FILE *f)
{
    if (f == NULL)
        return EOF; //Zadan neplatny popisovac souboru

    register int c;

    while ( (c=fgetc(f)) != EOF && isspace(c)) ; //preskocime uvodni isspace znaky

    if (c == EOF)
        return EOF;
    *s = c; //zapiseme prvni nacteny znak

    int i;
    for (i = 1; isspace((c = fgetc(f))) == 0; ++i)
    {
        if (c == EOF)
        {
            s[i] = '\0';
            return EOF;
        }
        if (i+1 >= max)
        {
            while (isspace(getc(f)) == 0) ; //zahodime zbytek slova
            fprintf(stderr, "Prilis dlouhe slovo, orezavam...\n");
            break; //konec nacitani, dosahli jsme limitu znaku
        }
        s[i] = c;
    }
    s[i] = '\0'; //nula ukoncujici retezec

    return i;
}


//!-----------------------------------------DEBUGGING_PART------------------------------------

//debugging cast, jako parametr programu chce jmeno souboru
#ifdef DEBUG_GETWORD
int main(int argc, char **argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "Zadej jako argument jmeno souboru.\n");
        return EXIT_FAILURE;
    }
    FILE *f = fopen(argv[1], "r");
    if (f == NULL)
    {
        fprintf(stderr, "Nelze otevrit soubor.\n");
        return EXIT_FAILURE;
    }

    char *s = malloc(100*sizeof(char));
    int d = fgetword(s, 10, f);

    printf("Retezec: %s|\nPocet znaku: %d\n", s, d);

    char *k = malloc(100*sizeof(char));
    int l = fgetword(k, 10, f);

    printf("Retezec: %s|\nPocet znaku: %d\n", k, l);

    fclose(f);
    free(s);
    free(k);

    return EXIT_SUCCESS;
}
#endif
