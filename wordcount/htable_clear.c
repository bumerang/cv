/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Implementace operaci s hashovaci tabulkou - htable_clear, cast b)
*/

#include "htable.h"


/**Vycisteni tabulky - vymaze vsechny polozky v tabulce
*Prijima: adresu tabulky (htable_t *)
*/
void htable_clear(htable_t *tableIn)
{
    if (tableIn == NULL || tableIn->htable_data == NULL)    //neni co rusit
        return;

    struct htable_listitem *tmpList = NULL;
    struct htable_listitem *tmpListNext = NULL;

    for (unsigned int i = 0; i < tableIn->htable_size; ++i)
    {
        if (tableIn->htable_data[i] == NULL)    //radek neni obsazeny, nemusime uvolnovat
            continue;

        tmpList = tableIn->htable_data[i];      //prvni prvek seznamu
        while (tmpList != NULL)             //zrusime vsechny prvky seznamu pro dany radek
        {
            if (tmpList->key != NULL)
                free(tmpList->key);     //uvolnime retezec aktualniho prvku
            tmpListNext = tmpList->next;    //prechodne uchovame adresu nasledujiho prvku
            if (tmpList != NULL)
                free(tmpList);      //zrusime aktualni prvek
            tmpList = tmpListNext;     //posouvame se na dalsi prvek
        }
        tableIn->htable_data[i] = NULL;     //zahodime ukazatel na dany radek seznamu
    }
}
