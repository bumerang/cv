/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: g++ 4.5.2
*Popis: Implementace POSIX programu tail v jazyce C++, cast a)
*/

#include <iostream>
#include <fstream> //fstream by mel byt zahrnut v iostream, pro jistotu
#include <deque>
#include <string>
#include <sstream>
#include <cstdlib>  //kvuli exit

const unsigned long LINES_COUNT = 10;    //vychozi pocet tisknutych radku

using namespace std;

void myTail(istream &streamIn, unsigned long linesCount, bool toEnd);
inline void FatalError(const char *msg);

int main(int argc, char **argv)
{
    fstream fileIn;
    istream *streamIn = &cin; //streamIn je pripojen na std. vstup
    bool toEnd = false;
    bool error = false;
    bool inFile = false;
    unsigned long linesCount = LINES_COUNT;
    string param;
    
    if (argc == 1)  //zadny parametr, cteme ze stdin
    {
        myTail(*streamIn, linesCount, toEnd);
        return 0;
    }
    else
    {
        if (argc == 2)  //Pr.: tail soubor.txt
        {
            fileIn.open(argv[1], ios::in);
            inFile = true;
        }
        else if (argc == 3 || argc == 4)
        {
            param = argv[1];
            if (param.compare("-n") == 0)   //Pr.: tail -n 30
            {
                param = argv[2];
                istringstream ssi (param, istringstream::in);   //stream pro string
                ssi >> linesCount;
                if (argv[2][0] == '+')  //tisk do konce radku
                    toEnd = true;
                if (argc == 4)      //Pr.: tail -n 30 soubor.txt
                {
                    fileIn.open(argv[3], ios::in);
                    inFile = true;
                }
            }
            else
                error = true;            
        }
        else
            error = true;
    }

    if (!fileIn.is_open() && inFile)
        FatalError("Nepodarilo se otevrit vstupni soubor");
    if (error || linesCount == 0) //nastala chyba (nepodarilo se nacist cislo)
    {
        fileIn.close();
        FatalError("Spatne zadane parametry");
    }
    if (inFile)
        streamIn = &fileIn;

    myTail(*streamIn, linesCount, toEnd);
/*
    //fileIn.open("pokus_text.txt", ios::in);
    if (!fileIn.is_open())
    {
        cerr << "Nepodarilo se otevrit vstupni soubor" << endl;
        return 1;
    }
    streamIn = &fileIn;

    myTail(*streamIn, LINES_COUNT, false);
*/
    fileIn.close();
    return 0;
}

void myTail(istream &streamIn, unsigned long linesCount, bool toEnd)
{
    deque<string> buffer;
    string lineIn;

    for (unsigned long i = 0; getline(streamIn, lineIn); ++i)
    {
        if (toEnd && i+1 >= linesCount) //tiskneme od zadaneho radku do konce
        {
            cout << lineIn << endl;
        }
        else        //nacitame zadany pocet radku
        {
        buffer.push_front(lineIn);
        if (i >= linesCount)        //prekroceni poctu vypisovanych radku
            buffer.pop_back();     //vyhodime nejstarsi radek
        }
    }

    if (!toEnd)     //tiskneme zadany pocet radku
    {
        while (!buffer.empty())     //dokud neni buffer prazdny
        {
            cout << buffer.back() << endl;  //tiskneme nejstarsi radek a potom ho zahodime
            buffer.pop_back();
        }
    }
}

inline void FatalError(const char *msg)
{
    cerr << msg << endl; //vypis zpravy na stderr
    exit(1);    //konec programu, errCode = 1
}
