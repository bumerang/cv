/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Implementace operaci s hashovaci tabulkou - htable_lookup, cast b)
*/

#include "htable.h"


/**Vyhleda zaznam v tabulce dle zadaneho klice - pokud takovy zaznam neexistuje,
*vytvori ho a zaradi do tabulky
*Prijima: adresu tabulky (htable_t *), vyhledavaci klic (const char *)
*Vraci: adresu zaznamu (struct htable_listitem *)
*       NULL pri neuspesne alokaci pameti
*/
struct htable_listitem *htable_lookup(htable_t *t, const char *key)
{
    if (t == NULL || key == NULL)
    {
        fprintf(stderr,"Chyba pri vyhledavani - nevalidni tabulka ci klic.\n");
        return NULL;
    }
    unsigned int hashIndex = hash_function(key, t->htable_size);     //ziskame hash pro dany klic
    //bool itemFound = false;

    struct htable_listitem *item = t->htable_data[hashIndex];       //zaciname na zacatku radku
    struct htable_listitem *itemPrevious = NULL;//neni zadny predchozi prvek

    while (item != NULL)    //postupne projdeme cely prislusny radek tabulky
    {
        if (strcmp(item->key, key) == 0)    //nalezli jsme prvek se stejnym klicem
        {
            ++item->data;       //zvysime pocet vyskytu klice
            return item;        //nalezli jsme prvek se stejnym klicem, vracime ukazatel
        }
        itemPrevious = item;
        item = item->next;
    }

    //prvek je unikatni, musime ho zaradit do tabulky

    struct htable_listitem *newItem = malloc(sizeof(*newItem));
    if (newItem == NULL)
        return NULL;

    newItem->key = malloc((strlen(key) + 1) * sizeof(char));     //alokujeme pamet pro retezec (+1 pro koncovou nulu)
    if (newItem->key == NULL)
    {
        free(newItem);
        return NULL;
    }

    (void) strcpy(newItem->key, key);      //kopirujeme predany klic do nove polozky, navratova hodnota strcpy nas nezajima
    newItem->data = 1;              //klic se vyskytuje prave jednou (prave ted)
    newItem->next = NULL;

    if (itemPrevious != NULL)     //pokud neni zaznam prvni v radku, pripojime ho za predchozi
        itemPrevious->next = newItem;
    else
        t->htable_data[hashIndex] = newItem;    //zaznam je prvni v radku

    return newItem;
}
