/**Projekt c.2 pro predmet IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Editovano: 28/8/2012
*Prelozeno: gcc 4.5.2
*Popis: Implementace POSIX prikazu tail v jazyce C99, cast a)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define CHARCOUNT 1024  //pocet znaku, vcetne ukoncovaci nuly a newline znaku
#define MAXFILENAME 255 //maximalni pocet znaku pro nazev souboru
#define LINESCOUNT 10   //pokud neni receno jinak, vypisuj 10 radku

enum errorState
{
    E_OK = 0,
    E_NOPARAM,
    E_BADPARAM,
    E_UNKNOWN,
};

//struktura pro uchovani dat ziskanych z parametru programu
typedef struct myParams
{
    unsigned long linesCount;
    bool toEnd;
    int state;
    FILE *fileIn;
} params_t;

void myTail(FILE *fileIn, unsigned long linesCount, bool toEnd);
params_t treatArg(int argc, char **argv);


int main(int argc, char **argv)
{
    params_t myParams = treatArg(argc, argv);
    if (myParams.state != E_OK)
    {
        fprintf(stderr, "Chyba pri zpracovani parametru.\n");
        return EXIT_FAILURE;
    }

    myTail(myParams.fileIn, myParams.linesCount, myParams.toEnd);

    if (myParams.fileIn != NULL)
        fclose(myParams.fileIn);

    return EXIT_SUCCESS;
}

//Zpracovani parametru, otevre pozadovany soubor
//Prijima: argumenty programu (int, char **)
//Vraci: parametrova struktura (params_t)
params_t treatArg(int argc, char **argv)
{
    params_t myParams = {.linesCount = LINESCOUNT, .toEnd = false, .state = E_OK, .fileIn = NULL };

    if (argc > 4)
    {
        myParams.state = E_BADPARAM;
        return myParams;
    }
    if (argc == 1) //zadne parametry, nacitat budeme ze vstupu (tzn. fileIn = NULL)
    {
        myParams.state = E_OK;
        return myParams;
    }
    else if (argc == 2)     //pr. formatu: tail soubor
    {
        myParams.fileIn = fopen(argv[1], "r");
        if (myParams.fileIn == NULL)
            myParams.state = E_BADPARAM;
        return myParams;
    }

    else if (strcmp(argv[1], "-n") == 0)    //pr. formatu: tail -n +3, argc == 3
    {
        char endCheck = 'P';    //sem ulozime prvni nevalidni znak pri prevodu pomoci strtoul
        char *endPtr = &endCheck;

        myParams.linesCount = strtoul(argv[2], &endPtr, 0);
        if ((*endPtr) == '\0')
            myParams.state = E_OK;      //cely retezec byl validni pro prevod
        else
            myParams.state = E_BADPARAM;

        if (argv[2][0] == '+')
            myParams.toEnd = true;
    }
    if (argc == 4)     //pr. formatu: tail -n +3 soubor
    {
        myParams.fileIn = fopen(argv[3], "r");
        if (myParams.fileIn == NULL)
            myParams.state = E_BADPARAM;
    }

    return myParams;
}

//Varianta POSIX tail-u, pokud je zadano '+', tiskne se do konce souboru od daneho radku
//Prijima: popisovac souboru, ze ktereho se bude cist(FILE *); pocet radek (unsigned long); priznak, zda tisknout do konce (bool)
void myTail(FILE *fileIn, unsigned long linesCount, bool toEnd)
{
    if (linesCount == 0)    //nula radku netiskneme
        return;

    if (fileIn == NULL)     //budeme nacitat ze std. vstupu
        fileIn = stdin;

    char buffer[linesCount][CHARCOUNT];     //variable length array, novinka v C99
    int c;                                  //pro kontrolu znaku

    if (toEnd == true)  //budeme tisknout od zadaneho radku do konce souboru
    {
        for (unsigned long lineCounter = 1; ; ++lineCounter)    //radky cislujeme od jednicky
        {
            if (fgets(buffer[0], CHARCOUNT, fileIn) == NULL)
                break;      //nastala chyba nebo konec souboru, konec nacitani
            if (strchr(buffer[0], '\n') == NULL)
            {
                fprintf(stderr, "Prilis dlouhy radek, orezavam\n");
                while ((c = getc(fileIn)) != '\n' && c != EOF) ; //zbytek radku zahazujeme
            }

            if (lineCounter >= linesCount)
                printf("%s", buffer[0]);     //vytiskneme radek
        }
        return;
    }

    unsigned long i;
    for (i = 0; ; ++i)
    {
        if (fgets(buffer[i % linesCount], CHARCOUNT, fileIn) == NULL)
        {
            //fprintf(stderr, "Chyba pri cteni radku\n");
            break;  //nastala chyba nebo konec souboru
        }

        if (strchr(buffer[i % linesCount], '\n') == NULL)
        {
            fprintf(stderr, "Prilis dlouhy radek, orezavam radek %lu\n",i+1);
            buffer[i % linesCount][CHARCOUNT-2] = '\n';  //pripojime znak newline, pro spravny tisk
            while ((c = getc(fileIn)) != '\n' && c != EOF) ;  //zbytek radku zahodime
        }
    }

    for (unsigned long j = 0 ; j < linesCount; ++i, ++j)     //tisk radku, aktualni prvek je "i" z prechoziho cyklu
    {
        printf("%s", buffer[i % linesCount]);
    }
}
