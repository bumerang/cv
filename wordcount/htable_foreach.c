/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Implementace operaci s hashovaci tabulkou - htable_foreach, cast b)
*/

#include "htable.h"


/**Nad kazdym prvkem (polozkou) tabulky provede predanou funkci
*Prijima: adresu tabulky (htable_t *),
*         ukazatel na (void) funkci, ktera prijima (const char *) a (unsigned int)
*/
void htable_foreach(htable_t *t, void (*function)(const char *, unsigned int))
{
    if (t == NULL)
    {
        fprintf(stderr, "Nevalidni tabulka -- f-ce foreach\n");
        return;
    }
    struct htable_listitem *item = NULL;

    for (unsigned int i = 0; i < t->htable_size; ++i)   //projdeme vsechny radky
    {
        item = t->htable_data[i];

        while (item != NULL)   //projdeme vsechny polozky daneho radku
        {
            function(item->key, item->data);    //postveme na dana data predanou funkci
            item = item->next;
        }
    }
}
