/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Rozhrani pro operace s hashovaci tabulkou, cast b)
*/

#ifndef HTABLE_H_INCLUDED
#define HTABLE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "hash_function.h"

struct htable_listitem
{
    char *key;                //ukazatel pro dynamicky alokovany retezec klice
    unsigned int data;        //pocet vyskytu klice
    struct htable_listitem *next;  //ukazatel na dalsi prvek
};

//!Kdyz pouzivam typedef, je nutno uvest za zaviraci slozenou zavorku } uvest jmeno!
//!Kdyz pouze definuji obecne strukturu, za zaviraci slozenou zavorkou } nebude nic!
typedef struct htable
{
    unsigned long htable_size;  //velikost pole ukazatelu
    struct htable_listitem *htable_data[];          //pole ukazatelu
} htable_t;

/**Nalezne a odstrani polozku se stejnym klicem, pokud takova polozka neexistuje, vypise chybu na stderr
*Prijima: adresu tabulky (htable_t *)
*         klic (const char *)
*/
void htable_remove(htable_t *t, const char *key);

/**Nad kazdym prvkem (polozkou) tabulky provede predanou funkci
*Prijima: adresu tabulky (htable_t *),
*         ukazatel na (void) funkci, ktera prijima (const char *) a (unsigned int)
*/
void htable_foreach(htable_t *t, void (*function)(const char *, unsigned int));

/**Vyhleda zaznam v tabulce dle zadaneho klice - pokud takovy zaznam neexistuje,
*vytvori ho a zaradi do tabulky
*Prijima: adresu tabulky (htable_t *), vyhledavaci klic (const char *)
*Vraci: adresu zaznamu (struct htable_listitem *)
*       NULL pri neuspesne alokaci pameti
*/
struct htable_listitem *htable_lookup(htable_t *t, const char *key);

/**Inicializace tabulky na danou velikost
*Prijima: velikost tabulky (unsigned int)
*Vraci: ukazatel na vytvorenou tabulku (htable_t *)
*       NULL pri neuspesnem vytvoreni */
htable_t *htable_init(unsigned int size);

/**Vycisteni tabulky - vymaze vsechny polozky v tabulce
*Prijima: adresu tabulky (htable_t *)
*/
void htable_clear(htable_t *tableIn);

/**Vycisti a zrusi celou tabulku
*Prijima: adresu tabulky (htable_t *)
*/
void htable_free(htable_t *tableIn);



#endif // HTABLE_H_INCLUDED




//!-------------------------------------------DEBUGGING_PART-----------------------------------------


//navratovou hodnotu mallocu neni treba pretypovavat - stejne implicitne konvertuje Z a NA void pointer
//lepsi nez:
//struct some *dexter = sizeof(struct some)
//je:
//  struct some *dexter = sizeof(*dexter)  -- nemusim opisovat definice typu, snadna pozdejsi modifikace

#ifdef DEBUG_HTABLE

#include "hash_function.h"

void funkce(const char *key, unsigned int value);

#define TAB_SIZE 10
int main(void)
{
    htable_t *myTable = htable_init(TAB_SIZE);

    for (unsigned int i = 0; i < TAB_SIZE; ++i)
    {
        //printf("iterace cislo %d\n", i);
        myTable->htable_data[i] = malloc (sizeof(*myTable->htable_data[i]));
        if (myTable->htable_data[i] == NULL)
        {
            printf("Something went terribly wrong. 1\n");
            exit(1);
        }
        myTable->htable_data[i]->key = malloc( 10 * sizeof(char));
        if (myTable->htable_data[i]->key == NULL)
        {
            printf("Something went terribly wrong. 2\n");
            exit(1);
        }
        strcpy(myTable->htable_data[i]->key, "zaznam");     //naplni vsechny klice slovem zaznamX je cislo
        myTable->htable_data[i]->key[6] = i + '0';  //prevod na char
        myTable->htable_data[i]->key[7] = '\0';
        myTable->htable_data[i]->data = 1;

        myTable->htable_data[i]->next = NULL;   //zatim mame pouze prvni cleny seznamu
    }

    printf("\n--------------------Dump tabulky s prvnimi 10 zaznamy-----------------------\n\n");
    for (unsigned int j = 0; j < TAB_SIZE; ++j)
    {
        printf("%d.klic tabulky: %s\n", j, myTable->htable_data[j]->key);
        printf("  Hash/index klice: %u\n", hash_function(myTable->htable_data[j]->key, TAB_SIZE));
        printf("  Ukazatel na dalsi prvek: %p\n\n", (void *)myTable->htable_data[j]->next);
    }

    //Pridavani zaznamu do tabulky
    (void) htable_lookup(myTable, "Dr. Walter Bishop");
    (void) htable_lookup(myTable, "Sherlock Holmes");
    (void) htable_lookup(myTable, "Jack Reacher");
    (void) htable_lookup(myTable, "Nikola Tesla");
    (void) htable_lookup(myTable, "Alan Turing");
    (void) htable_lookup(myTable, "Claude Shannon");
    (void) htable_lookup(myTable, "Richard Feynman");
    (void) htable_lookup(myTable, "Massive Dynamic");
    (void) htable_lookup(myTable, "Nick Twisp");
    (void) htable_lookup(myTable, "Frank Sinatra");
    (void) htable_lookup(myTable, "Frank Sinatra");
    (void) htable_lookup(myTable, "Grace Kelly");
    (void) htable_lookup(myTable, "Grace Kelly");
    (void) htable_lookup(myTable, "Grace Kelly");

for (int k = 0; k < 2; ++k)
{
    //dump tabulky
    printf("\n--------------------------Dump tabulky-----------------------------\n");
    for (int radek = 0; radek < TAB_SIZE; ++radek)
    {
        struct htable_listitem *walter = myTable->htable_data[radek];
        printf("\nKlice pro radek %d:\n",radek);
        while (walter != NULL)
        {
            printf("  |%s|  %s", walter->key, walter->next == NULL ? "--|" : "-->");
            walter = walter->next;
        }
    }

    printf("\n\n---------Ukazka volani funkce nad tabulkou - htable_foreach--------------\n\n");
    htable_foreach(myTable, funkce);
    
    if (k == 0)
    {
        printf("\n-----------------------Odstraneni polozek--------------------------\n\n");
        printf("Odstranuji nasledujici polozky:\n  Sherlock Holmes\n  Richard Feynman\n");
        htable_remove(myTable, "Sherlock Holmes");
        htable_remove(myTable, "Richard Feynman");
    }
}

    htable_free(myTable);


    return EXIT_SUCCESS;
}

//testovaci funkce, pouze vytiskne hodnoty
void funkce(const char *key, unsigned int value)
{
    printf("Klic: %s\t\tpocet vyskytu: %u\n", key, value);
}

#endif //DEBUG_HTABLE
