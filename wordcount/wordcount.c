/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Prelozeno: gcc 4.5.2
*Popis: Hlavni program pro pocitani slov + jejich vypis, cast b)
*Poznamka: Zadnou funkci se nevyplati inlinovat
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "htable.h"
#include "io.h"

#define MAX_WORD 255   //255 znaku na slovo
const unsigned int TAB_SIZE = 24593;
/*Velikost tabulky je optimalne (pro rychlost - operaci % lze nahradit za bitovy soucin pro ty druhe operandy, ktere jsou mocninou dvojky)
* mocnina dvou pro kvalitni hash-funkci - pokud vsak neni hash funkce kvalitni, snadno zacne dochazet ke shlukovani zaznamu
* na urcitych indexech. Pokud vsak volime prvocislo, toto shlukovani zaznamu se primerene zmensi (i pro mene kvalitni hashovaci funkci).
*Zdroje: http://srinvis.blogspot.com/2006/07/hash-table-lengths-and-prime-numbers.html
*        http://planetmath.org/encyclopedia/GoodHashTablePrimes.html
*/

//Funkce pro tisk obsahu polozky
//Prijima: retezec k tisku (const char *), cislo k tisku (unsigned int)
void printContent(const char *key, unsigned int value);


int main(void)
{
    htable_t *myTable = htable_init(TAB_SIZE);
    if (myTable == NULL)
    {
        fprintf(stderr, "Chyba pri alokaci pameti pro tabulku.\n");
        return EXIT_FAILURE;
    }

    char wordIn[MAX_WORD+1] = {'\0'};       //vstupni buffer pro nacitana slova
    while (fgetword(wordIn, MAX_WORD, stdin) != EOF)    //nacitame slova, dokud nenastane chyba ci konec souboru
    {
         if (htable_lookup(myTable, wordIn) == NULL)    //Pridame zaznam ci inkrementujeme pocitadlo
         {
             fprintf(stderr, "Chyba pri alokaci pameti pro zaznam.\n");
             htable_free(myTable);
             return EXIT_FAILURE;
         }
    }
    htable_foreach(myTable, printContent);      //na kazdou polozku postveme funkci pro tisk

    htable_free(myTable);   //uvolnime celou tabulku

    return EXIT_SUCCESS;
}

//Funkce pro tisk obsahu polozky
//Prijima: retezec k tisku (const char *), cislo k tisku (unsigned int)
void printContent(const char *key, unsigned int value)
{
    printf("%s\t%u\n", key, value);
}
