/**Projekt c.2 do predmetu IJC - Jazyk C
*Autor: Petr Jirout - 1BIT, VUT FIT
*       xjirou07@stud.fit.vutbr.cz
*Datum: 4/2012
*Popis: Rozhrani pro vhodnou rozptylovaci funkci pro hash tabulku, cast b)
*Poznamka: Opsano ze zadani
*/

#ifndef HASH_FUNCTION_H_INCLUDED
#define HASH_FUNCTION_H_INCLUDED


#include <stdio.h>
#include <stdlib.h>

unsigned int hash_function(const char *str, unsigned htable_size);



#endif // HASH_FUNCTION_H_INCLUDED
