/**
Nazev: Projekt c. 2 do predmetu IPK -- Pocitacove komunikace a site,
       Program pro vyhledani informaci o uzivatelich unixoveho OS
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 4.3.2013
Modul: interface.hpp
*/

#ifndef INTERFACE_H_INCLUDED
#define INTERFACE_H_INCLUDED

#include <string>
#include <vector>

#define FORMAT_TRUE  '1'
#define FORMAT_FALSE '0'
#define FORMAT_DELIMITER ':'
#define FORMAT_ENDLINE '\n'
#define FORMAT_ENDLINE_SIZE 1
#define FORMAT_POSITION_DELIM ';'

//Format chybove zpravy: ERROR_STRING + ERROR_PREFIX_{LOGIN,UID} + {LOGIN,UID}
//unikatnost zajistena tim, ze v odpovedi na korektni dotaz se nesmi vyskytovat dvojtecka

//konstanty chybovych zprav, pri zmene FORMAT_DELIMITER je treba je patricne upravit
extern const char* ERROR_STRING;
extern const unsigned int ERROR_STRING_LENGTH;
extern const char* ERROR_PREFIX_LOGIN;
extern const char* ERROR_PREFIX_UID;


//struktura pro uchovani informaci o pozadovanem vysledku hledani
typedef struct
{
    bool searchByUID;
    bool searchByLogin;
    bool getLogin;
    bool getUID;
    bool getGID;
    bool getGecos;
    bool getHome;
    bool getShell;
    int getLoginPosition;
    int getUIDPosition;
    int getGIDPosition;
    int getGecosPosition;
    int getHomePosition;
    int getShellPosition;
    std::vector<std::string> UIDVector;
    std::vector<std::string> loginVector;
} searchParams_t;


int CreateMessage(const searchParams_t& search, std::string& message);
int DecodeMessage(const std::string& message, searchParams_t& search);


#endif //INTERFACE_H_INCLUDED
