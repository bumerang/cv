/**
Nazev: Projekt c. 2 do predmetu IPK -- Pocitacove komunikace a site,
       Program pro vyhledani informaci o uzivatelich unixoveho OS
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 4.3.2013
Modul: interface.cpp
*/

#include "interface.hpp"

#include <sstream>
#include <iostream>
#include <cstdlib>

//konstanty chybovych zprav, pri zmene FORMAT_DELIMITER je treba je patricne upravit
const char* ERROR_STRING = ":error:";
const unsigned int ERROR_STRING_LENGTH = 7;
const char* ERROR_PREFIX_LOGIN = "login:";
const char* ERROR_PREFIX_UID = "UID:";

//prevede zpravu v prenosovem formatu do parametrove struktury
int DecodeMessage(const std::string& message, searchParams_t& search)
{
    //vynuluji predanou strukturu
    search.loginVector.clear();
    search.UIDVector.clear();
    search.searchByLogin = false;
    search.searchByUID = false;
    search.getGID = false;
    search.getGecos = false;
    search.getHome = false;
    search.getLogin = false;
    search.getShell = false;
    search.getUID = false;

    //zkontroluji spravny format
    for (size_t i = 1, len = message.find(FORMAT_POSITION_DELIM); i < len; i += 2)
    {
        if (message[i] != FORMAT_DELIMITER || (message[i-1] != FORMAT_TRUE && message[i-1] != FORMAT_FALSE))
            return 1;
    }

    //postupne projdu cely formatovaci retezec zpravy a aktivuji prislusne flagy
    if (message[0] == FORMAT_TRUE) //search by UID
        search.searchByUID = true;
    if (message[2] == FORMAT_TRUE) //search by login
        search.searchByLogin = true;
    if (message[4] == FORMAT_TRUE) //getLogin
        search.getLogin = true;
    if (message[6] == FORMAT_TRUE) //getUID
        search.getUID = true;
    if (message[8] == FORMAT_TRUE) //getGID
        search.getGID = true;
    if (message[10] == FORMAT_TRUE) //getGecos
        search.getGecos = true;
    if (message[12] == FORMAT_TRUE) //getHome
        search.getHome = true;
    if (message[14] == FORMAT_TRUE) //getShell
        search.getShell = true;

    //std::stringstream sa (message.substr(message.find(FORMAT_POSITION_DELIM)+1, message.find(FORMAT_ENDLINE) -
    //                                                                            message.find(FORMAT_POSITION_DELIM)+1));

    //zpracuji cast s cisly pozic jednotlivych argumentu
    char pom[2] = {'\0'};
    pom[0] = message[16];
    search.getLoginPosition = atoi(pom);
    pom[0] = message[18];
    search.getUIDPosition = atoi(pom);
    pom[0] = message[20];
    search.getGIDPosition = atoi(pom);
    pom[0] = message[22];
    search.getGecosPosition = atoi(pom);
    pom[0] = message[24];
    search.getHomePosition = atoi(pom);
    pom[0] = message[26];
    search.getShellPosition = atoi(pom);


    std::string temp;

    std::stringstream ss (message.substr(message.find(FORMAT_ENDLINE)+FORMAT_ENDLINE_SIZE));

    while (ss >> temp)
    {
        if (search.searchByUID)
            search.UIDVector.push_back(temp);
        else
            search.loginVector.push_back(temp);

    }

    return 0;
}


//vytvori spravny format zpravy pro zaslani serveru
int CreateMessage(const searchParams_t& search, std::string& message)
{
    message.clear();

    if (search.searchByUID)
    {
        message += FORMAT_TRUE;
        message += FORMAT_DELIMITER;
        message += FORMAT_FALSE;
    }
    else if (search.searchByLogin)
    {
        message += FORMAT_FALSE;
        message += FORMAT_DELIMITER;
        message += FORMAT_TRUE;
    }
    else //spatny format search
        return 1;

    //naplnim prvni radek formatu (co chci vyhledat)
    message += FORMAT_DELIMITER;
    if (search.getLogin)
        message += FORMAT_TRUE;
    else
        message += FORMAT_FALSE;

    message += FORMAT_DELIMITER;
    if (search.getUID)
        message += FORMAT_TRUE;
    else
        message += FORMAT_FALSE;

    message += FORMAT_DELIMITER;
    if (search.getGID)
        message += FORMAT_TRUE;
    else
        message += FORMAT_FALSE;

    message += FORMAT_DELIMITER;
    if (search.getGecos)
        message += FORMAT_TRUE;
    else
        message += FORMAT_FALSE;

    message += FORMAT_DELIMITER;
    if (search.getHome)
        message += FORMAT_TRUE;
    else
        message += FORMAT_FALSE;

    message += FORMAT_DELIMITER;
    if (search.getShell)
        message += FORMAT_TRUE;
    else
        message += FORMAT_FALSE;

    //nyni vlozim cast s cisly umisteni pozic argumentu
    message += FORMAT_POSITION_DELIM;
    message += search.getLoginPosition + '0';
    message += FORMAT_DELIMITER;
    message += search.getUIDPosition + '0';
    message += FORMAT_DELIMITER;
    message += search.getGIDPosition + '0';
    message += FORMAT_DELIMITER;
    message += search.getGecosPosition + '0';
    message += FORMAT_DELIMITER;
    message += search.getHomePosition + '0';
    message += FORMAT_DELIMITER;
    message += search.getShellPosition + '0';

    //na nove radky postupne dam vsechny vyhledavaci klice (loginy ci UID)
    if (search.searchByUID)
    {
        for (std::vector<std::string>::const_iterator it = search.UIDVector.begin(); it != search.UIDVector.end(); ++it)
        {
            message += FORMAT_ENDLINE;
            message += *it;
        }
    }
    else if (search.searchByLogin)
    {
        for (std::vector<std::string>::const_iterator it = search.loginVector.begin(); it != search.loginVector.end(); ++it)
        {
            message += FORMAT_ENDLINE;
            message += *it;
        }
    }
    else //spatny format
        return 1;

    return 0;
}

