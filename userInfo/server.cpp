/**
Nazev: Projekt c. 2 do predmetu IPK -- Pocitacove komunikace a site,
       Program pro vyhledani informaci o uzivatelich unixoveho OS
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 4.3.2013
Modul: server.cpp
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cctype>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include <cstdlib>

#include "interface.hpp"

const char* DATABASE_FILENAME = "/etc/passwd";
const unsigned int BUFFER_SIZE = 1024;
const unsigned int MAX_THREAD_COUNT = 20;
const unsigned int THREADPOOL_ITEM_FREE = 1;
const unsigned int THREADPOOL_ITEM_BUSY = 0;


//struktura pro predani informaci vlaknu pro obsluhu klienta
typedef struct
{
    int FD;
    unsigned int poolIndex;
    pthread_mutex_t mutex;
    pthread_t threadPool[MAX_THREAD_COUNT];
    unsigned int threadPoolStatus[MAX_THREAD_COUNT];
} clientInfo_t;

//globalni promenne pro korektni osetreni ukonceni pomoci signalu
clientInfo_t clientInfo;
int socketFD;


int GetSearchedLine(std::string& result, searchParams_t& search);
void* TreatClient(void* arg);
void SignalHandler(int signum);


int main(int argc, char** argv)
{
    //registrace obsluznych funkci pro prichozi signaly
    signal(SIGINT, SignalHandler);
    signal(SIGTERM, SignalHandler);

    //zkontroluji parametry
    if (argc != 3 || (strcmp(argv[1], "-p") != 0))
    {
        std::cerr << "Invalid arguments\n";
        return 1;
    }
    for (unsigned int i = 0, len = strlen(argv[2]); i < len; ++i)
    {
        if (! isdigit(argv[2][i])) //zadana ne-cislice v port number
        {
            std::cerr << "Invalid arguments (port no.)\n";
            return 1;
        }
    }

    struct addrinfo hints;
    struct addrinfo* result;

    memset(&hints, 0, sizeof(hints)); //vynulovani
    hints.ai_family = AF_INET; //IPv4
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; //socket bude pasivni (schopen akceptovat pripojeni na nej)
    hints.ai_protocol = IPPROTO_TCP;

    int s = getaddrinfo(NULL, argv[2], &hints, &result);
    if (s != 0)
    {
        std::cerr << "Address error: " << gai_strerror(s) << std::endl;
        return 1;
    }

    struct addrinfo *rp;

    //zkousim se pripojovat na ziskane adresy, dokud neprojdu vsechny ci se nepripojim
    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        socketFD = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (socketFD == -1)
            continue;

        if (bind(socketFD, rp->ai_addr, rp->ai_addrlen) != -1)
            break; //uspech, pripojeno

        close(socketFD);
    }

    freeaddrinfo(result); //socket je pripojeny, adresy jiz nebudu potrebovat

    if (rp == NULL)
    {
        std::cerr << "Creating socket has failed\n";
        close(socketFD);
        return 1;
    }

    if (listen(socketFD, 0) == -1) //nastavim socket na naslouchaci mod
    {
        std::cerr << "Setting socket to the listening mode has failed\n";
        close(socketFD);
        return 1;
    }

    struct sockaddr theirAddr;
    socklen_t slen;

    //inicializace
    clientInfo.mutex = PTHREAD_MUTEX_INITIALIZER;
    for (unsigned int i = 0; i < MAX_THREAD_COUNT; ++i)
        clientInfo.threadPoolStatus[i] = THREADPOOL_ITEM_FREE;

    while (true) //nekonecny cyklus pro prijimani spojeni/zadosti
    {
        clientInfo.FD = accept(socketFD, static_cast<struct sockaddr*>(&theirAddr), &slen);
        if (clientInfo.FD == -1)
        {
            std::cerr << "Error has occured when accepting connection\n";
            SignalHandler(1); //ukonci program
        }

        pthread_mutex_lock(&clientInfo.mutex); //zamknu pristup ke strukture nez bude vyrizen aktualni klient

        //cekam dokud se neuvolni misto v bazenu vlaken
        for (unsigned int i = 0; ; (i == MAX_THREAD_COUNT) ? i = 0 : ++i)
        {
            if (clientInfo.threadPoolStatus[i] == THREADPOOL_ITEM_FREE)
            {
                clientInfo.threadPoolStatus[i] = THREADPOOL_ITEM_BUSY;
                clientInfo.poolIndex = i;
                break;
            }
        }
        //vytvorim vlakno
        pthread_create(&clientInfo.threadPool[clientInfo.poolIndex], NULL, TreatClient, static_cast<void*>(&clientInfo));
    }

    return 0;
}

//Obslouzi klienta, funkce pro vlakno
//Prijima: ukazatel na strukturu (clientInfo_t*)
void* TreatClient(void* arg)
{
    clientInfo_t* clientInfo = static_cast<clientInfo_t*>(arg);
    unsigned int poolIndex = clientInfo->poolIndex;
    int FD = clientInfo->FD;

    pthread_mutex_unlock(&clientInfo->mutex); //odemknu pristup pro dalsiho klienta

    char buffer[1] = {'\0'};
    std::string str;
    ssize_t nread;
    while ((nread = read(FD, buffer, 1)) != -1)
    {
        if (buffer[0] == '\0') //konec cteni
            break;

        str.append(buffer, nread);
    }

    searchParams_t search;
    if (DecodeMessage(str, search) != 0)
    {
        std::cerr << "Error has occured during message decoding\n";
        return NULL;
    }

    std::string response;
    if (GetSearchedLine(response, search) != 0) //vyhledam odpoved na pozadavek
    {
        std::cerr << "Error has occured when searching the record\n";
        return NULL;
    }

    //poslu odpoved zpet klientovi
    write(FD, response.c_str(), response.size()+1);

    close(FD);
    clientInfo->threadPoolStatus[poolIndex] = THREADPOOL_ITEM_FREE; //konec vlakna, nastavuji na volne misto
    return NULL;
}

//osetri ukoncujici signaly
//Prijima: cislo signalu
void SignalHandler(int signum)
{
    //cekam dokud vsechna vlakna nejsou korektne ukoncena
    for (unsigned int i = 0; i < MAX_THREAD_COUNT; ++i)
    {
        if (clientInfo.threadPoolStatus[i] == THREADPOOL_ITEM_BUSY)
            pthread_join(clientInfo.threadPool[i], NULL);
    }

    close(socketFD);

    exit(signum); //ukonci program s danym exit kodem signalu
}

//otevre soubor a vrati pozadovana data
int GetSearchedLine(std::string& result, searchParams_t& search)
{
    std::ifstream inputFile (DATABASE_FILENAME, std::ifstream::in); //otevru soubor pro cteni

    if (! inputFile.is_open())
    {
        return 1;
    }

    char buffer[BUFFER_SIZE] = {'\0'}; //pomocny buffer pro nacitani radku
    std::string searchStr;             //retezec, ktery aktualne hledam
    std::string str;                   //pomocny retezec pro prochazi souboru
    bool found = false;

    if (search.searchByLogin)
        searchStr = search.loginVector.front();
    else if (search.searchByUID)
        searchStr = search.UIDVector.front();

    //hledam do te doby, dokud nevyprazdnim odpovidajici vektor hledanych zaznamu
    while ((search.searchByLogin && !search.loginVector.empty()) ||
           (search.searchByUID && !search.UIDVector.empty()) )
    {
        str.clear();

        do //nactu radek
        {
            inputFile.getline(buffer, BUFFER_SIZE);
            str.append(buffer, inputFile.gcount());
        } while (inputFile.gcount() == BUFFER_SIZE);

        if (inputFile.eof()) //precetl jsem cely soubor a nic jsem nenasel
        {
            if (! found)
            {
                //zapisuji chybovou zpravu v predepsanem formatu
                result.append(ERROR_STRING);
                if (search.searchByLogin)
                    result.append(ERROR_PREFIX_LOGIN);
                else
                    result.append(ERROR_PREFIX_UID);
                result.append(searchStr + "\n");
            }

            inputFile.clear();  //vynuluje eofbit
            inputFile.seekg(0); //pristi hledani bude probihat opet od zacatku souboru

            if (search.searchByLogin)
            {
                search.loginVector.erase(search.loginVector.begin()); //odstranim prvni prvek
                if (! search.loginVector.empty())
                {
                    searchStr = search.loginVector.front();
                    continue;
                }
            }
            else //searchByUID
            {
                search.UIDVector.erase(search.UIDVector.begin()); //odstranim prvni prvek
                if (! search.UIDVector.empty())
                {
                    found = false;
                    searchStr = search.UIDVector.front();
                    continue;
                }
            }
            break;
        }

        //parsing file
        if (search.searchByLogin)
        {
            if (searchStr.compare(str.substr(0,str.find(':'))) == 0) //nasel jsem radek s danym zaznamem
            {
                result.append(str); //vlozim tuto radku do vysledneho retezce
                result.append("\n");
                search.loginVector.erase(search.loginVector.begin()); //odstranim prvni prvek
                if (! search.loginVector.empty())
                {
                    searchStr = search.loginVector.front();
                    inputFile.clear();  //vynuluje eofbit
                    inputFile.seekg(0); //pristi hledani bude probihat opet od zacatku souboru
                    continue;
                }
                break;
            }
        }
        else if (search.searchByUID)
        {
            unsigned int colonCount = 0;
            for (unsigned int i = 0, len = str.length(); i < len; ++i)
            {
                if (str[i] == ':')
                    ++colonCount;
                if (colonCount == 2) //za druhou dvojteckou zacina cast UID
                {
                    ++i; //posunu se na prvni prvek za druhou dvojteckou
                    searchStr = search.UIDVector.front();
                    if (searchStr.compare(str.substr(i, str.find(':',i)-i)) == 0) //nasel jsem hledany element
                    {
                        result.append(str); //vlozim tuto radku do vysledneho retezce
                        result.append("\n");
                        found = true;
                    }
                }
            }
        }
        else //predana vyhledavaci struktura je invalidni
            return 1;
    }
    int positionMax = 0;
    if (positionMax < search.getGIDPosition)
        positionMax = search.getGIDPosition;
    if (positionMax < search.getGecosPosition)
        positionMax = search.getGecosPosition;
    if (positionMax < search.getHomePosition)
        positionMax = search.getHomePosition;
    if (positionMax < search.getLoginPosition)
        positionMax = search.getLoginPosition;
    if (positionMax < search.getShellPosition)
        positionMax = search.getShellPosition;
    if (positionMax < search.getUIDPosition)
        positionMax = search.getUIDPosition;

    str = result;      //zkopiruji si vsechny radky z vypisu a ty budu zpracovavat
    result.clear();    //do result uz prijdou data ve spravnem formatu
    std::string temp;
    //extrahuji pozadovane informace z retezce
    for (size_t prevEndline = -1, endlineIndex = str.find('\n'); endlineIndex != std::string::npos;
         prevEndline = endlineIndex, endlineIndex = str.find('\n', endlineIndex+1))
    {
        temp = str.substr(prevEndline+1, endlineIndex - prevEndline); //v temp je aktualni radek i s '\n'
        //std::cerr << "actualLine: |" << temp << "|";
        if (temp.compare(0, ERROR_STRING_LENGTH, ERROR_STRING) == 0) //retezec s chybou kopiruji
        {
            result.append(temp);
            continue; //jdu zpracovavat dalsi retezec
        }


        //projdu aktualni radek (zaznam)
        int position = 1;
        for (unsigned int j = 0, len = temp.length(), colonCount = 0; j < len; ++j)
        {
            if (j == len-1 && position != positionMax+1)
            {
                j = 0;
                colonCount = 0;
            }
            if (temp[j] == ':')
            {
                if (j+1 != len && temp[j+1] == ':')
                {
                    if (position == search.getGIDPosition && search.getGIDPosition == 0)
                        ++position;
                    else if (position == search.getGecosPosition && search.getGecosPosition == 0 )
                        ++position;
                    else if (position == search.getHomePosition && search.getHomePosition == 0)
                        ++position;
                    else if (position == search.getLoginPosition && search.getLoginPosition == 0)
                        ++position;
                    else if (position == search.getShellPosition && search.getShellPosition == 0)
                        ++position;
                    else if (position == search.getUIDPosition && search.getUIDPosition == 0)
                        ++position;
                }
                if (j != 0 && colonCount != 0 && result.size() != 0 &&
                    result[result.length()-1] != ' ' && result[result.size()-1] != '\n') //pridam oddelujici mezeru
                    result += ' ';
                ++colonCount;
                continue;
            }
            if (search.getLogin && colonCount == 0 && position == search.getLoginPosition) //kopiruji login, znak po znaku
            {
                result += temp[j];
                if (j+1 == len || temp[j+1] == ':')
                    ++position;

            }
            else if (search.getUID && colonCount == 2 && position == search.getUIDPosition)
            {
                result += temp[j];
                if (j+1 == len || temp[j+1] == ':')
                    ++position;
            }
            else if (search.getGID && colonCount == 3 && position == search.getGIDPosition)
            {
                result += temp[j];
                if (j+1 == len || temp[j+1] == ':')
                    ++position;
            }
            else if (search.getGecos && colonCount == 4 && position == search.getGecosPosition)
            {
                if (j+1 == len || temp[j+1] == ':')
                    ++position;
                //pridal jsem carku navic za cely Gecos
                if (j+1 < len && temp[j+1] == ':' && result.size() > 0 && result[result.size()-1] == ',')
                {
                    result.erase(result.length()-1);
                    continue;
                }
                if (temp[j] == ',' && result[result.length()-1] == ',') //prebytecne carky nevkladam
                    continue;
                result += temp[j];
            }
            else if (search.getHome && colonCount == 5 && position == search.getHomePosition)
            {
                result += temp[j];
                if (j+1 == len || temp[j+1] == ':')
                    ++position;
            }
            else if (search.getShell && colonCount == 6 && position == search.getShellPosition)
            {
                result += temp[j];
                if (j+1 == len || temp[j+1] == ':' || temp[j+1] == '\n')
                    ++position;
            }
            else if (j == len -1)
                result += temp[j]; //kopiruji konec radku
        }
        if (result[result.size()-1] == ' ') //pridal jsem mezeru navic na konec radku, pryc s ni
            result.erase(result.length()-1, 1);

    }
        //odstranim zanesene nuly
        size_t del;
        int ind = -1;
        while ((del = result.find('\0', ind+1)) != std::string::npos)
        {
            result.erase(del, 1);
            ind = del;
        }

    return 0;
}
