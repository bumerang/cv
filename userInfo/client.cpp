/**
Nazev: Projekt c. 2 do predmetu IPK -- Pocitacove komunikace a site,
       Program pro vyhledani informaci o uzivatelich unixoveho OS
Autor: Petr Jirout - 2BIT, VUT FIT
       xjirou07@stud.fit.vutbr.cz
Datum: 4.3.2013
Modul: client.cpp
*/

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "interface.hpp"


//struktura pro uchovani informaci potrebnych pro pripojeni
typedef struct
{
    std::string port;
    std::string host;
} connectInfo_t;


int TreatParams(char** argv, connectInfo_t& conInfo, searchParams_t& search);

int main(int argc, char** argv)
{
    if (argc < 4)
    {
        std::cerr << "Invalid arguments\n";
        return 1;
    }
    connectInfo_t conInfo;
    searchParams_t search;
    if (TreatParams(argv, conInfo, search) != 0)
    {
        std::cerr << "Invalid arguments\n";
        return 1;
    }

    struct addrinfo hints;
    struct addrinfo* result;

    memset(&hints, 0, sizeof(hints)); //vynulovani
    hints.ai_family = AF_INET; //IPv4
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = IPPROTO_TCP;

    int s = getaddrinfo(conInfo.host.c_str(), conInfo.port.c_str(), &hints, &result);
    if (s != 0)
    {
        std::cerr << "Address error: " << gai_strerror(s) << std::endl;
        return 1;
    }

    int socketFD;
    struct addrinfo *rp;

    //zkousim se pripojovat na ziskane adresy, dokud neprojdu vsechny ci se nepripojim
    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        socketFD = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (socketFD == -1)
            continue;

        if (connect(socketFD, rp->ai_addr, rp->ai_addrlen) != -1)
            break; //uspech, pripojeno

        close(socketFD);
    }
    //nemusim se bindovat na port, OS mi nejaky prideli automaticky

    if (rp == NULL)
    {
        std::cerr << "Error during establishing connection\n";
        return 1;
    }

    freeaddrinfo(result);

    //prevedu parametrovou strukturu do formatu pro odesilani serveru
    std::string message;
    if (CreateMessage(search, message) != 0)
    {
        std::cerr << "Error during message creation\n";
        return 1;
    }

    //odeslani pozadavku na server vcetne koncove nuly
    write(socketFD, message.c_str(), message.size()+1);

    char buffer[1] = {'\0'};
    std::string answer;
    ssize_t nread;
    while ((nread = read(socketFD, buffer, 1)) != -1)
    {
        if (buffer[0] == '\0') //konec cteni
            break;

        answer.append(buffer, nread);
    }

    std::string line;
    std::stringstream ss (answer);

    //postupne projdu vsechny radky
    while (std::getline(ss, line))
    {
        if (line.compare(0, ERROR_STRING_LENGTH, ERROR_STRING) == 0) //zaznam nebyl nalezen
        {
            if (line.find(ERROR_PREFIX_LOGIN) != std::string::npos) //hledal jsem login
                std::cerr << "Chyba: neznamy login " << line.substr(line.find_last_of(FORMAT_DELIMITER)+1) << std::endl;
            else
                std::cerr << "Chyba: nezname UID " << line.substr(line.find_last_of(FORMAT_DELIMITER)+1) << std::endl;
        }
        else
            std::cout << line << std::endl; //zaznam je v poradku, tisknu ho

    }

    close(socketFD);

    return 0;
}


//zpracuje parametry a naplni prislusne struktury
int TreatParams(char** argv, connectInfo_t& conInfo, searchParams_t& search)
{
    bool hostInserted = false;
    bool portInserted = false;
    search.searchByLogin = false;
    search.searchByUID = false;
    search.getGID = false;
    search.getGecos = false;
    search.getHome = false;
    search.getLogin = false;
    search.getShell = false;
    search.getUID = false;
    search.getUIDPosition = 0;
    search.getGIDPosition = 0;
    search.getGecosPosition = 0;
    search.getHomePosition = 0;
    search.getLoginPosition = 0;
    search.getShellPosition = 0;

    std::string temp;
    unsigned int position = 1;
    for (unsigned int i = 1; argv[i] != NULL; ++i) //projdu vsechny argumenty
    {
        if (argv[i][0] == '-')
        {
            for (unsigned int j = 1, len = strlen(argv[i]); j < len; ++j)
            {
                if (argv[i][j] == 'L')
                {
                    search.getLogin = true;
                    search.getLoginPosition = position++;
                }
                else if (argv[i][j] == 'U')
                {
                    search.getUID = true;
                    search.getUIDPosition = position++;
                }
                else if (argv[i][j] == 'G')
                {
                    search.getGID = true;
                    search.getGIDPosition = position++;
                }
                else if (argv[i][j] == 'N')
                {
                    search.getGecos = true;
                    search.getGecosPosition = position++;
                }
                else if (argv[i][j] == 'H')
                {
                    search.getHome = true;
                    search.getHomePosition = position++;
                }
                else if (argv[i][j] == 'S')
                {
                    search.getShell = true;
                    search.getShellPosition = position++;
                }
                else if (argv[i][j] == 'l')
                {
                    if ((j != 1 && len != 2) || argv[++i] == NULL )
                        return 1;
                    while (argv[i] != NULL && argv[i][0] != '-') //nactu do vektoru vsechny loginy k vyhledani
                    {
                        temp = argv[i++];
                        search.loginVector.push_back(temp);
                    }
                    --i; //posunul jsem index parametru o pozici navic
                    search.searchByLogin = true;
                    search.searchByUID = false;
                    break;

                }
                else if (argv[i][j] == 'u')
                {
                    if ((j != 1 && len != 2) || argv[++i] == NULL )
                        return 1;
                    while (argv[i] != NULL && argv[i][0] != '-') //nactu do vektoru vsechny UID k vyhledani
                    {
                        temp = argv[i++];
                        search.UIDVector.push_back(temp);
                    }
                    --i; //posunul jsem index parametru navic
                    search.searchByLogin = false;
                    search.searchByUID = true;
                    break; //jdu zpracovavat dalsi parametr
                }
                else if (argv[i][j] == 'h')
                {
                    if ((j != 1 && len != 2) || argv[++i] == NULL )
                        return 1;
                    conInfo.host = argv[i];
                    hostInserted = true;
                    break; //jdu zpracovavat dalsi parametr
                }
                else if (argv[i][j] == 'p')
                {
                    if ((j != 1 && len != 2) || argv[++i] == NULL )
                        return 1;
                    for (unsigned int k = 0, len = strlen(argv[i]); k < len; ++k)
                    {
                        if (! isdigit(argv[i][k])) //byla zadana ne-cislice v cisle portu
                        {
                        std::cerr << "err1\n";
                            return 1;
                        }
                    }
                    conInfo.port = argv[i];
                    portInserted = true;
                    break; //jdu zpracovavat dalsi parametr
                }
                else //chybne parametry
                {
                    std::cerr << "err2\n";
                    return 1;
                }
            }
        }
        else //chyba parametru
        {
            std::cerr << "err3\n";
            return 1;
        }

    }
    //nebyly zadany vsechny povinne parametry
    if (! (hostInserted && portInserted && (search.searchByLogin || search.searchByUID)))
        return 1;

    return 0;
}
